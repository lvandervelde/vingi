$(document).ready(function(){ 
    $('.gallery:not(.noDetail)').overlay();
    
    $('.outerWrapper.hidden > img').load(function(){
    	$(this).fadeIn(100, function(){
            $(this).parents('.hidden').removeClass('hidden');
        });
    }).error(function(){
    	$(this).attr("src", "http://vingi.nl/assets/img/notFound.png");
    });
    
    initGroupEdit();
    initMediaEdit();
});

function initMediaEdit(){

    $('#mediaEditForm button[type=submit]').click(function(){
        $('#submit_button').val($(this).attr('name'))
    })
}

function isVisible(elem)
{
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}

/**
 * Initialize the buttons to move photos from and to group albums
 */
function initGroupEdit(){
    $('#albumGrid .outerWrapper').click(function(evt) {
    	evt.preventDefault();
    	$(this).toggleClass("albumImageSelected");
    });
    
    $('#groupGrid .outerWrapper').click(function(evt) {
    	evt.preventDefault();
    	$(this).toggleClass("albumImageSelected");
    });
    
	var urlParts = document.URL.split('/');
	var albumId = urlParts[urlParts.length - 1];
    
    $('#addToAlbum').click(function() {
    	
    	var data = new Object();
    	var medias = new Array();
    	
    	$('#groupGrid').find($('.albumImageSelected')).each(function() {
    		
    		var wrapper = $(this)
    		
    		var mediaUnique = wrapper.children('img').attr('id');
    		var userUnique = wrapper.children('img').attr('userId');
    		
    		medias.push({'media':mediaUnique, 'user':userUnique});
    		
    		wrapper.removeClass('albumImageSelected');
    		wrapper.appendTo('#albumGrid');
    		
    	});
    	
    	data.album = albumId;
    	data.medias = medias;
    	
    	post_return('album/addMediaToAlbum', JSON.stringify(data));
    });

    $('#removeFromAlbum').click(function() {
    		
    	var data = new Object();
    	var medias = new Array();
    	
    	$('#albumGrid').find($('.albumImageSelected')).each(function() {
    		
    		var wrapper = $(this)
    		
    		var mediaUnique = wrapper.children('img').attr('id');
    		var userUnique = wrapper.children('img').attr('userId');
    		
    		medias.push({'media':mediaUnique, 'user':userUnique});
    		
    		wrapper.removeClass('albumImageSelected');
    		wrapper.appendTo('#groupGrid');
    		
    	});
    	
    	data.album = albumId;
    	data.medias = medias;
    	
    	post_return('album/removeMediaFromAlbum', JSON.stringify(data));
    });  
}

/**
 * Do an ajax post and return the response
 */
function post_return(url, dataString) {
    var response;

    $.ajax({
        type: "POST",
        url: root+url,
        async: false,
        data: dataString,
        success: function(msg) {
            response = msg;
        },
        error: function (request, status, error) {
            return (request.responseText);
        }
    });

    return (response);
}

/**
 * Load a select list into an element
 */
(function($) {
    $.fn.overlay = function(parameters){

        var element = $(this);

        //set the options, extended by the given parameters
        var options = $.extend ({
        }, parameters);
        
        element.children('.outerWrapper').each(function(){
            $(this).unbind('hover');
            $(this).mouseenter(
                function () {
                    $('.imageDetails').hide();
                    
                    $(this).find('.imageDetails').fadeIn(90);
                    
                    $(this).find('.contentWrapper').mouseleave(function(){
                        $('.imageDetails').hide();
                    });
                }
            );
        });

        return element;
    }
})(jQuery);

function getFileName(file) {
    return file.name.substr(0,file.name.lastIndexOf("."));
}

function readableFileSize(size) {
    var units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    var i = 0;
    while(size >= 1024) {
        size /= 1024;
        ++i;
    }
    return size.toFixed(1) + ' ' + units[i];
}

/**
 * Load a select list into an element
 */
(function($) {
    $.fn.formSelect = function(parameters){

        var element = $(this);

        //set the options, extended by the given parameters
        var options = $.extend ({
            dataUrl: '',
            urlParam: '',
            idColumn: '',
            labelColumn: '',
            selectedId: '',
            visibleOptions: 1,
            disabled: false,
            multiple: false,
            hasEmptyLabel: true,
            emptyLabel: "Selecteer een optie"
        }, parameters);

        //clear the html element
        element.html('');
        element.attr("size", options.visibleOptions);

        if(options.disabled){
            element.attr("disabled","disabled");
        }
        if(options.multiple){
            element.attr("multiple","multiple");
        }
        if(options.hasEmptyLabel){
            element.append($("<option />").val(0).text(options.emptyLabel));
        }

        //get the data from the given data url with the given parameters
        var data = post_return(root+options.dataUrl, options.urlParam);

        //parse the data and put ito the html element
        $($.parseJSON(data)).each(function() {
            element.append($("<option />").val(this[options.idColumn]).text(this[options.labelColumn]));
        }); 
        //set the selected value
        element.val(options.selectedId);

        return element;
    }
})(jQuery);
