/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
    
//    $('.mediaWrapper .icon').click(function(){
//        var mediaid = $('.fullscreen').attr('mediaid');
//                
//        $("#albumSelect").formSelect({
//            dataUrl: "user/getAlbums",
//            idColumn: "album.id",
//            labelColumn: "album.name",
//            hasEmptyLabel: false
//        });
//        
//        $("#albumSelect").fadeIn(500);
//    })
//    
//    $('.mediaWrapper .userIcon').click(function(){
//        var mediaid = $('.fullscreen').attr('mediaid');
//        
//        $('#groupSelect')
//    })
    
    $('.fullscreen').click(function(){
        getFullScreenImage( $(this) );
    });
    
    $("#mediaTagCloud").jQCloud(getTags( $("#mediaTagCloud").attr("media") ));

    $('#commentSubmit').click(function(){
        addComment();
    });
    
    initModify();
});

function initModify(){
    $('.modify').unbind();
    $('.modify').click(function(){
        editComment( $(this) );
    });
}

function getFullScreenImage( icon ){
    var mediaid = icon.attr('mediaid');  
    window.location.replace(root+"media/fullscreen/"+mediaid);
}

function addComment(){
    var response = post_return('media/addComment', $('#commentCreate').serialize()); 
        
    $('#comments').append(response);
    $('#commentCreateBody').text('');
    initModify();
}

function editComment( thisClick ){
    var body = thisClick.parent().parent().find('.body');

    /** Replace the p with a textarea **/
    var p = body.find("p:first");
    p.replaceWith($("<textarea/>", {
        "class": "edit",
        "text": p.text().replace("\n", "").replace(/\s{2,}/g, " ").trim(),
        "css": { "width": p.css('width') }
    }));

    /** Add the submit button to the body field **/
    if(body.find('.button').length == 0){
        var button = $('<div>').html('Change').addClass('button').css('width', '85px').click(function(){
            editSubmit( $(this) ); 
        });

        body.append( button );
    }
}

function editSubmit( button ){
    var textarea = button.parent().find('.edit');
    var data = ({
        id: button.parent().prop('id'),
        text: textarea.val()
    });
    
    var response = JSON.parse( post_return('media/editComment', data) );
    
    $('#'+data.id+' .editdate').html( 'Last edit: '+response.date );
    button.remove();
    
    var p = $("<p/>").text( response.text ).addClass('text');
    textarea.replaceWith(p);
}

/**
 * Get tags for a media
 */
function getTags(mediaId){
    return JSON.parse( post_return('media/getTags', 'mediaId='+mediaId) );
}



