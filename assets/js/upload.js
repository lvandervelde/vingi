            
$(document).ready(function(){
    initUploadButtons();
    initUploadArea();
    
    jQuery.event.props.push("dataTransfer");
    
    $('body').bind("dragenter", dragEnter);
    //$('body').bind("dragexit", dragExit);
    $('body').bind("dragend", dragExit);
    $('body').bind("dragover", ignore);
    $('body').bind("drop", drop);    
});

function ignore(evt) {
	evt.stopPropagation();
	evt.preventDefault();
}

function dragExit(evt) {
	ignore(evt);
	
    $('.highlightUploader').removeClass('highlightUploader');
    
}

function dragEnter(evt) {	
	  ignore(evt);
	  
	  $('#mediaUploader').addClass('highlightUploader');  
}

function drop(evt) {
	ignore(evt);
	  
	$('.highlightUploader').removeClass('highlightUploader'); 
	
	handleFileSelect(evt);
}

var filesToUpload = new Array();
var filesToRead = new Array();
var imageBlocks = new Array();
var uploading = 0;
var reading = 0;

function initUploadButtons()
{
    $('#uploadButtons').buttonset();
    
    $('#addPhoto').click(function(){
        $('#fileInput').click();
    });
    
    $('#removePhoto').click(function(){
        $('#mediaUploader .selected').closest('.imageBlock').each(function(){
        	imageBlocks
        	
        });
        
        if( $('#mediaUploader .imageBlock').length == 0 )
        {
            $('#mediaUploaderText').show()
        }
    });
    
    $('#ok').click(function(){
        $('#fileUploadForm').submit();
    });
    
    $('#cancel').click(function(){
        var text = $('#mediaUploaderText');
        $('#mediaUploader').html(text);
        $('#mediaUploaderText').show()
        initUploadArea();
    });
}

function initUploadArea()
{
    $('#mediaUploader button').click(function(){
        $('#fileInput').click();
    })
   
    $('#fileInput').change(function(evt){
    	handleFileSelect(evt);
    });
    
    $("#fileUploadForm").submit(function(){

    	$(".imageBlock:not(.done)").addClass("uploading");
    	
    	handleUpload();
    	
    	return false;
    });
}

function handleUpload(){

	//alert("length: "+filesToUpload.length);
	console.debug("length: "+filesToUpload.length);
	while(uploading < 3 && filesToUpload.length > 0){
		uploading++;
		var file = filesToUpload.shift();
		var imageBlock = imageBlocks.shift();
		uploadFile(file, imageBlock);
	}
	
	if(filesToUpload.length > 0){
		setTimeout('handleUpload()', 200);
	}
}

/**
 * Handle the selected files in the upload input field.
 * Show a preview of each of the selected files
 * @param evt - Change event of the input field
 */
function handleFileSelect(evt) 
{
	if (typeof evt.dataTransfer != "undefined" && evt.dataTransfer.files && evt.dataTransfer.files[0]) 
    {
        var files = evt.dataTransfer.files;
        
        for (var i = 0, file; file = files[i]; i++) 
        {
        	filesToRead.push(file);
        }
    } else if (typeof evt.target != "undefined" && evt.target.files && evt.target.files[0]) 
    {
        var files = evt.target.files;
        
        for (var i = 0, file; file = files[i]; i++) 
        {
        	filesToRead.push(file);
        }
    }
	readFiles();
}

/**
 * Setup the filereader and show the file preview
 * after the onload event.
 * @param file - The file to show
 */
function readFiles() 
{
	console.log("FilesToRead: "+filesToRead.length);
	if(filesToRead.length == 0)
		return;
	
	if(reading > 3){
		setTimeout("readFiles()", 100);
	}
	
	reading++;
	
	var file = filesToRead.shift();
	
    var reader = new FileReader();  
    reader.onload = function(event) {  
        var imageBlock = showPreview(event, file);
        
    	filesToUpload.push(file);
    	imageBlocks.push(imageBlock)
    }
    reader.readAsDataURL(file);
    
    if(reading > 3){
		setTimeout("readFiles()", 100);
	} else {
		readFiles();
	}
}

/**
 * Show a preview of the picture in the upload field.
 * @param event - The event from the filereader
 * @param file - The file to show
 */
function showPreview(event, file)
{
	reading--;
	
    var imageBlock = $('<div class="imageBlock">');
    var image = $('<div class="image">');
    var progressBar = $('<div class="progressBar">');
    var progress = $('<div class="progress animation">');
    
    progressBar.append(progress);
    
    image.click(function(){
        $(this).find('img').toggleClass('selected'); 
    }).append('<img src="'+event.target.result+'" title="'+getFileName(file)+'" />');
    
    var text = $('<div class="text">'+getFileName(file)+'</div>')
    makeEditable(text);
    
    var filename = $('<div class="filename">').html(file.name);
    
    imageBlock.append(image).append(text).append(filename).append(progressBar);
    
    $('#mediaUploaderText').hide();
    $('#mediaUploader').append(imageBlock);
    
    return imageBlock;
}

/**
 * Make a text editable with an input field.
 */
function makeEditable(element)
{
    element.click(function(){
    	if($(this).parents(".imageBlock").is(".done,.uploading"))
    		return;
        var input = $('<input>').val( $(this).html() ).blur(function(){
            $(this).parent().html( $(this).val() );
            makeEditable(element);
        })
        
        $(this).html( input ).unbind('click')
        
        input.focus();
    });
}

/**
 * Toggle the active state of the remove button.
 * Depends on the availability of selected images.
 */
function toggleRemoveButton()
{
    if($('#mediaUploader .selected').length > 0)
    {
        $("#removePhoto").removeAttr("disabled").removeClass("ui-state-disabled");
    }
    else
    {
        $('#removePhoto').prop("disabled", true).addClass("ui-state-disabled");
    }  
}




function uploadFile(file, imageBlock) {

	var groupId = $('#groupIdVal').val();
	var albumId = $('#albumIdVal').val();
	var name = $(imageBlock).find(".text").text();
	var fd = new FormData();
	fd.append("groupId", groupId);
	fd.append("albumId", albumId);
	fd.append("name", name);
	fd.append("file", file);
	
    var xhr = new XMLHttpRequest();
    xhr.upload.addEventListener("progress", function(evt){
    	//console.debug(imageBlock);
    	var progressBar = $(imageBlock).find(".progressBar");
    	if (evt.lengthComputable) {
	      var percentComplete = Math.round(evt.loaded * 100 / evt.total);
	      progressBar.find(".progress").width(percentComplete + "%");
	    }
	    else {
	      document.getElementById('progressNumber').innerHTML = 'unable to compute';
	    }
    }, false);
    xhr.addEventListener("load", function(evt){
    	console.debug(evt.target.response);
    	$(imageBlock).removeClass("uploading").addClass("done");
    	$(imageBlock).find(".progress").removeClass("animation");
    	uploading--;
    }, false);
    xhr.addEventListener("error", uploadFailed, false);
    xhr.addEventListener("abort", uploadCanceled, false);
    xhr.open("POST", "/media/uploadProgress");
    xhr.send(fd);
  }

  function uploadFailed(evt) {
    alert("There was an error attempting to upload the file.");
    uploading--;
  }

  function uploadCanceled(evt) {
    alert("The upload has been canceled by the user or the browser dropped the connection.");
    uploading--;
  }

