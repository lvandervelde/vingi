
$(document).ready(function(){
    initFilter();
    loadAll();
    
    $( "#moreMedia" ).click(function(){
        appendMedia( $('#latestUploads') );
        appendMedia( $('#popularUploads') );
    });
    
//    $(window).scroll(function(){
//    	if($(".outerWrapper").length < 50 && isScrolledIntoView($("#moreMedia"))){
//            appendMedia( $('#latestUploads') );
//            appendMedia( $('#popularUploads') );
//    	}
//    });
    
    setInterval('prependMedia()', 4000);
    
});

/**
 * Periodically check for new uploaded media and 
 * prepend it to the current media on screen.
 */
function prependMedia(){
    var element = $('#latestUploads');
    var url = 'home/getMedia';
    var filter = getFilters();
    var mediaId = getFirstMediaId( element );
    
    if( mediaId ){
        filter.media = mediaId;
    }
    
    filter.operator = '>';
    
    asyncPost(url, filter, element, true);    
};

function isScrolledIntoView(elem)
{
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}

/**
 * After a filter is changed all media will be removed,
 * and mediaId equals false. Otherwise the 'Get more' 
 * button is clicked and only media older then the last
 * one should be added.
 */
function appendMedia( element ){
    var url = 'home/getMedia';
    var filter = getFilters();
    var mediaId = getLastMediaId( element );
    
    //'Get more' button is clicked, so mediaId is true
    if( mediaId ){
        filter.media = mediaId;
    }
    
    //PopularUploads has diffirent filter settings
    if( element.attr('id') == 'popularUploads') {        
        filter.limit = 8;
        filter.orderby = 'media.views';
    }
    
    asyncPost(url, filter, element);
};

/**
 * Get the mediaId from the first media 
 * object on the screen.
 * @return mediaId || false
 */
function getFirstMediaId( element ){
    if( element.find('.outerWrapper').length > 0 ){
        return element.find('.outerWrapper:first').find('img:first').attr('mediaid');
    }else{
        return false;
    }
};

/**
 * Get the mediaId from the last media 
 * object on the screen.
 * @return mediaId || false
 */
function getLastMediaId( element ){
     if( element.find('.outerWrapper').length > 0 ){
        return element.find('.outerWrapper:last').find('img:first').attr('mediaid');
    }else{
        return false;
    }
};

/**
 * Get the filter values
 * @return filter object
 */
function getFilters(){
    
    var filter = ({
        search: $('#searchInput').val(),
        group: $( "#groupSelect" ).val(),
        media: false,
        limit: 20,
        orderby: 'media.id',
        operator: '<'
    });
    
    return filter;
};

/**
 * Initialize the filter function
 */
function initFilter(){
    var timeout;
    
    $('#searchInput').bind('keyup', function(){
        clearTimeout(timeout);
        timeout = setTimeout("loadAll()", "500");
    });
    
    $( "#groupSelect" ).change(function(){
        loadAll();
    });
};

/**
 * Reload all media after a filter changes
 */
function loadAll(){
    clearMedia();
    
    appendMedia( $('#latestUploads') );
    appendMedia( $('#popularUploads') );
}

/**
 * Remove all media from the screen before reloading.
 */
function clearMedia(){
    $('#latestUploads').html($('#latestUploads').find("h2"));
    $('#popularUploads').html($('#popularUploads').find("h2"));
}

/**
 * Do an ajax post and return the response
 */
function asyncPost(url, dataString, element, prepend) {
    var response;

    $.ajax({
        type: "POST",
        url: root+url,
        data: dataString,
        success: function(msg) {
            response = msg;
            
            if(typeof element != 'undefined' && !prepend){
                element.append( response );
                element.overlay();
            }
            if(response && prepend){
                element.find('h2:first').after( response );
                element.overlay();
            }
            
            $('.outerWrapper.hidden > img').load(function(){
                $(this).fadeIn(100, function(){
                    $(this).parents('.hidden').removeClass('hidden');
                });
            }).error(function(){
                $(this).attr("src",root + "/assets/img/notFound.png");
            });
        },
        error: function (request, status, error) {
            return (request.responseText);
        }
    });

    return (response);
}