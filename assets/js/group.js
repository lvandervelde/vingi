
$(document).ready(function(){
    initFilter();
    loadAll();
    
    $( "#moreMedia" ).click(function(){
        appendMedia( $('#latestUploads') );
        appendMedia( $('#popularUploads') );
    });
    
    setInterval('prependMedia()', 4000);
    
});

/**
 * Periodically check for new uploaded media and 
 * prepend it to the current media on screen.
 */
function prependMedia(){
    var element = $('#latestUploads');
    var url = 'home/getMedia';
    var filter = getFilters();
    var mediaId = getFirstMediaId( element );
    
    if( mediaId ){
        filter.media = mediaId;
    }
    
    filter.operator = '>';
    
    var media = post_return(url, filter);
    if(media){
        element.find('h2:first').after( media );
        element.overlay();
    }
};

/**
 * After a filter is changed all media will be removed,
 * and mediaId equals false. Otherwise the 'Get more' 
 * button is clicked and only media older then the last
 * one should be added.
 */
function appendMedia( element ){
    var url = 'home/getMedia';
    var filter = getFilters();
    var mediaId = getLastMediaId( element );
    
    //'Get more' button is clicked, so mediaId is true
    if( mediaId ){
        filter.media = mediaId;
    }
    
    //PopularUploads has diffirent filter settings
    if( element.attr('id') == 'popularUploads') {
        var latestAmount = $('#latestUploads').children('.outerWrapper').length;
        var popularAmount = element.children('.outerWrapper').length;
        
        filter.limit = 8;
        filter.orderby = 'media.views';
    }
    
    element.append( post_return(url, filter) );
    element.overlay();
};

/**
 * Get the mediaId from the first media 
 * object on the screen.
 * @return mediaId || false
 */
function getFirstMediaId( element ){
    if( element.find('.outerWrapper').length > 0 ){
        return element.find('.outerWrapper:first').find('img:first').attr('mediaid');
    }else{
        return false;
    }
};

/**
 * Get the mediaId from the last media 
 * object on the screen.
 * @return mediaId || false
 */
function getLastMediaId( element ){
     if( element.find('.outerWrapper').length > 0 ){
        return element.find('.outerWrapper:last').find('img:first').attr('mediaid');
    }else{
        return false;
    }
};

/**
 * Get the filter values
 * @return filter object
 */
function getFilters(){
    
    var filter = ({
        search: $('#searchInput').val(),
        group: $( "#groupSelect" ).val(),
        media: false,
        limit: 20,
        orderby: 'media.id',
        operator: '<'
    });
    
    return filter;
};

/**
 * Initialize the filter function
 */
function initFilter(){
    var timeout;
    
    $('#searchInput').bind('keyup', function(){
        clearTimeout(timeout);
        timeout = setTimeout("loadAll()", "500");
    });
    
    $( "#groupSelect" ).change(function(){
        loadAll();
    });
};

/**
 * Reload all media after a filter changes
 */
function loadAll(){
    clearMedia();
    
    appendMedia( $('#latestUploads') );
    appendMedia( $('#popularUploads') );
}

/**
 * Remove all media from the screen before reloading.
 */
function clearMedia(){
    $('#latestUploads').html($('#latestUploads').find("h2"));
    $('#popularUploads').html($('#popularUploads').find("h2"));
}