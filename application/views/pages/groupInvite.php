<div class="row pageTitle"><!-- open row -->
  <div class="twelvecol"><!-- open column -->
    <h1><img src="<?php echo URL::base(); ?>assets/img/invite.png" alt="Invite some people"> Invite people for <?= $group->name?></h1>
  </div><!-- close column -->
</div><!-- close row -->

<div class="row"><!-- open row -->
  <div class="twelvecol"><!-- open column -->
    <form id="formInvite" action="<?= url::base(); ?>group/sendInvite" method="post"><!-- open #formLogin -->
      <fieldset>
        <legend>Email Address</legend>

        <div>
          <label for="inviteEmail">Email</label>
          <input id="inviteEmail" class="text" name="invite[email]" type="text" placeholder="Email">
        </div>
         <div>
          <label for="inviteDescription">Description</label>
          <textarea id="inviteDescription" name="invite[description]" placeholder="Description">I have invited you to join my group: "<?= $group->name?>" on Vingi.</textarea>
        </div>

        <input type="hidden" name="invite[id]" value="<?=$group->id?>">
        <input name="invite[submit]" type="submit" value="Invite">
      </fieldset>
    </form><!-- close #formLogin -->
  </div><!-- close column -->
</div><!-- close row -->