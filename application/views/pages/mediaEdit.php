<br>
<div class="row">
	<div class="eightcol mediaView">

		<div class="mediaWrapper">
			<img class="media"
					 src="<?php echo $media->getMediaPath('small', $user); ?>"
					 title="<?php echo sprintf('%s: %s', $media->name, $media->description); ?>"
					 alt="<?php echo $media->description; ?>">
			<div class="title"><?php echo $media->name; ?></div>
			<div class="mediaDate"> <?php echo $media->creationDate; ?></div>
		</div>

	</div>
	<div class="fourcol last">

		<form id="mediaEditForm" class="form" action="<?= url::base() ?>media/editProcess" method="post">

			<div class="mediaInfo">
				<div class="title">Media name <span>(max 25 characters)</span></div>
				<input name="media[name]" type="text" maxlength="25" onclick="select()" placeholder="Media name" value="<?= $media->name ?>">
			</div>

			<div class="mediaInfo">
				<div class="title">Media description <span>(max 35 words)</span></div>
				<textarea name="media[description]" placeholder="Media description"><?= $media->description ?></textarea>
			</div>

			<div class="mediaInfo">
				<div class="title">Media tags <span>(comma separated)</span></div>
				<textarea name="media[tags]" placeholder="Media tags"><?= $media->getTagString() ?></textarea>
			</div>

			<input type="hidden" name="media[id]" value="<?= $media->mediaUnique ?>">
			<input type="hidden" id="submit_button" name="media[submit]">

			<button name="delete" type="submit" class="button">Delete</button>
			<button name="cancel" type="submit" class="button">Cancel</button>
			<button name="submit" type="submit" class="button">Submit</button>
			<!--<button name="next" type="submit" class="button">Next</button>-->
		</form>

	</div>
</div>
<br>