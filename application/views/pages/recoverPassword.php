<div class="row pageTitle"><!-- open row -->
  <div class="twelvecol"><!-- open column -->
    <h1>Recover password</h1>
  </div><!-- close column -->
</div><!-- close row -->

<div class="row"><!-- open row -->
  <div class="twelvecol"><!-- open column -->
    <form id="formRecoverPassword" action="#" method="post"><!-- open #formRecoverPassword -->
      <div>
        <label for="recoverEmail">Email</label>
        <input id="recoverEmail" class="text" name="recoverPassword[email]" type="email" placeholder="Email address">
      </div>

      <input name="recoverPassword[submit]" type="submit" value="Send">
    </form><!-- close #formRecoverPassword -->
  </div><!-- close column -->
</div><!-- close row -->