<!--Thumbnail Navigation-->
<div id="prevthumb"></div>
<div id="nextthumb"></div>

<!--Arrow Navigation-->
<a id="prevslide" class="load-item"></a>
<a id="nextslide" class="load-item"></a>

<div id="thumb-tray" class="load-item">
    <div id="thumb-back"></div>
    <div id="thumb-forward"></div>
</div>

<!--Time Bar-->
<div id="progress-back" class="load-item">
    <div id="progress-bar"></div>
</div>

<!--Control Bar-->
<div id="controls-wrapper" class="load-item">
    <div id="controls">

        <a id="play-button"><img id="pauseplay" src="<?=url::base()?>assets/img/pause.png"></a>

        <!--Slide counter-->
        <div id="slidecounter">
            <span class="slidenumber"></span> / <span class="totalslides"></span>
        </div>

        <!--Slide captions displayed here-->
        <div id="slidecaption"></div>

        <!--Thumb Tray button-->
        <a id="tray-button"><img id="tray-arrow" src="<?=url::base()?>assets/img/button-tray-up.png"></a>

        <!--Navigation-->
        <ul id="slide-list"></ul>

        <div id="exit">Close</div>

    </div>
</div>

<style type="text/css">
    html, body, .container, #main{
        background: none;
        border: none;
    }

    html{
        background: #000;
    }

    #exit{
        float: right;
        color: #FFF;
        margin: 8px 15px 0px 0px;
    }

    #exit:hover{
        cursor:pointer;
        text-decoration: underline;
    }

    #header, #footer{
        display: none;
    }

    ul#demo-block{ margin:0 15px 15px 15px; }
    ul#demo-block li{ margin:0 0 10px 0; padding:10px; display:inline; float:left; clear:both; color:#aaa; background:url('img/bg-black.png'); font:11px Helvetica, Arial, sans-serif; }
    ul#demo-block li a{ color:#eee; font-weight:bold; }

    #tray-button,
    #play-button{
        display: none;
    }
</style>

<script type="text/javascript">

    jQuery(function($){

        var response = post_return('media/getFullscreenMedia', 'id='+<?php echo $start; ?>);
        var response = response.split("::");
        var start = (parseInt(response[1])+1);
        var medias = JSON.parse(response[0]);

        $.supersized({

            // Functionality
            slideshow               :   1,			// Slideshow on/off
            autoplay                :	0,			// Slideshow starts playing automatically
            start_slide             :   start,                  // Start slide (0 is random)
            stop_loop               :	0,			// Pauses slideshow on last slide
            random                  : 	0,			// Randomize slide order (Ignores start slide)
            slide_interval          :   3000,                   // Length between transitions
            transition              :   6, 			// 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
            transition_speed        :	1000,                   // Speed of transition
            new_window              :	1,			// Image links open in new window/tab
            pause_hover             :   0,			// Pause slideshow on hover
            keyboard_nav            :   1,			// Keyboard navigation on/off
            performance             :	0,			// 0-Normal, 1-Hybrid speed/quality, 2-Optimizes image quality, 3-Optimizes transition speed // (Only works for Firefox/IE, not Webkit)
            image_protect           :	1,			// Disables image dragging and right click with Javascript

            // Size & Position
            min_width		    :   0,			// Min width allowed (in pixels)
            min_height		    :   0,			// Min height allowed (in pixels)
            vertical_center         :   1,			// Vertically center background
            horizontal_center       :   1,			// Horizontally center background
            fit_always              :	1,			// Image will never exceed browser width or height (Ignores min. dimensions)
            fit_portrait            :   1,			// Portrait images will not exceed browser height
            fit_landscape           :   1,			// Landscape images will not exceed browser width

            // Components
            slide_links             :	'blank',                // Individual links for each slide (Options: false, 'num', 'name', 'blank')
            thumb_links             :	0,			// Individual thumb links for each slide
            thumbnail_navigation    :   0,			// Thumbnail navigation
            slides                  :  	medias,

            // Theme Options
            progress_bar            :	0,			// Timer for each slide
            mouse_scrub             :	0

        });

        $('#exit').click(function(){
            window.location.replace( post_return('media/getPath', 'id='+<?php echo $start; ?>) );
        })
    });

</script>


