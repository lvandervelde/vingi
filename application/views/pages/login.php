<div class="row form"><!-- open .row -->
  <div class="sixcol"><!-- open .sixcol -->
    <img src="<?php echo URL::base(); ?>assets/img/logo.png" title="Vingi" alt="Vingi logo">
  </div><!-- close .sixcol -->

  <div class="sixcol last"><!-- open .sixcol -->
    <form id="formLogin" action="login/verify" method="post"><!-- open #formLogin -->
      <fieldset>
        <legend>Login</legend>

        <div>
          <label for="loginEmail">Email</label>
          <input id="loginEmail" class="text" name="login[email]" type="text" placeholder="Email">
        </div>

        <div>
          <label for="loginPassword">Password</label>
          <input id="loginPassword" class="text" name="login[password]" type="password" placeholder="Password">
        </div>

        <input name="login[submit]" type="submit" value="Login">
      </fieldset>
    </form><!-- close #formLogin -->
  </div><!-- close .sixcol -->
</div><!-- close .row -->