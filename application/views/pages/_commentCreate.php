<form id="commentCreate" action="<?php echo URL::base(); ?>media/addComment" method="post"><!-- open #commentCreate -->
  <input name="comment[mediaId]" type="hidden" value="<?php echo $media->id; ?>">

  <fieldset>
    <legend>Add new comment</legend>

    <div>
      <textarea id="commentCreateBody" rows="10" name="comment[content]" placeholder="Write your comment"></textarea>
    </div>

    <button type="button" class="button" id="commentSubmit">Submit</button>
  </fieldset>
</form><!-- close #commentCreate -->
