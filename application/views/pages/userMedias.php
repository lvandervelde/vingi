<div class="row pageTitle"><!-- open row -->
    <div class="threecol"><!-- open column -->
        <img id="userImage" src="<?php echo URL::base(); ?><?= $user->getAvatar($user) ?>" title="<?= $user->firstname ?> <?= $user->lastname ?>" alt="Profile picture of user <?= $user->firstname ?> <?= $user->lastname ?>">
    </div><!-- close column -->

    <div class="ninecol last"><!-- open column -->
        <div id="profileHeader"><!-- open #profileHeader -->
            <h1><?= $user->firstname ?> <?= $user->lastname ?></h1>
        </div><!-- close #profileHeader -->
    </div><!-- close column -->
</div><!-- close row -->

<div class="row overflow"><!-- open row -->
    <div class="twelvecol"><!-- open column -->
        <div id="latestUploads" class="gallery">
            <h2>Uploads</h2>
            <?php echo View::factory('pages/_photoGallery')->bind('medias', $uploads) ?><br>
        </div>        
    </div><!-- close column -->
</div><!-- close row -->

<div class="row"><!-- open row -->
    <div class="twelvecol"><!-- open column -->
        <!-- To fix the overlow hidden problem with the previous row -->
    </div><!-- close column -->
</div><!-- close row -->
