<div id="filters"><!-- open #filters -->
  <input class="filter text" name="search" id="searchInput" type="text" placeholder="Find uploads">

  <select id="groupSelect" name="group" class="filter">
    <option value="0">All groups</option>
    <?php
    foreach ($userGroups as $group):
      echo "<option value='" . $group->id . "'>" . $group->name . "</option>";
    endforeach;
    ?>
  </select>

  <div id="viewType" class="filter" style="display: none">
    <input type="radio" id="photoView" name="liveStreamView" value="0" checked="checked">
    <label for="photoView">Photos</label>
    <input type="radio" id="albumView" name="liveStreamView" value="1">
    <label for="albumView">Albums</label>
  </div>
</div><!-- close #filters -->