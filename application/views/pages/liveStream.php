
<div class="row pageTitle"><!-- open row -->
    <div class="twelvecol"><!-- open column -->
        <h1>Live stream</h1>
    </div><!-- close column -->
</div><!-- close row -->

<div class="row overflow"><!-- open row -->

    <div class="threecol"></div>
    
    <div class="sixcol"><!-- open column -->
        <?php echo View::factory('pages/_filters')->bind('userGroups', $userGroups) ?>
    </div><!-- close column -->
    
    <div class="threecol last"></div>
</div>

<div id="uploadContent" class="row overflow"><!-- open #uploadContent / open row -->
    <div class="eightcol"><!-- open column -->

        <div id="latestUploads" class="gallery"><!-- open #latestUploads -->
            <h2>Latest uploads</h2>
            <!-- loaded through ajax -->
        </div><!-- close #latestUploads -->
        
    </div><!-- close column -->

    <div class="fourcol last"><!-- open column -->

        <div id="popularUploads" class="gallery"><!-- #popularUploads -->
            <h2>Popular uploads</h2>
            <!-- loaded through ajax -->
        </div><!-- close #popularUploads -->

    </div><!-- close column -->
</div><!-- close #uploadContent / close row -->

<div class="row"><!-- open row -->
    <div class="twelvecol"><!-- open column -->
        
        <button class="button" id="moreMedia">Get more media</button>
        <!-- To fix the overlow hidden problem with the previous row -->
    </div><!-- close column -->
</div><!-- close row -->
