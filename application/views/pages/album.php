<div class="row pageTitle"><!-- open row -->
	<div class="threecol"><!-- open column -->
		<img src="<?= $cover->getMediaPath('small'); ?>" title="<?= $album->name ?>" alt="Cover image of album <?= $album->name ?>">
	</div><!-- close column -->

	<div class="sevencol"><!-- open column -->
		<h1>
			<?= $album->name; ?>
			<?php if ($edit): ?>
				<a href="<?= URL::base(); ?>album/edit/<?= $album->id ?>">
					<img src="<?php echo URL::base(); ?>assets/img/modify.png">
				</a>
			<?php endif; ?>
		</h1>
		<ul>
			<li>
				<span class="label">Part of</span>
				<a href="<?php echo URL::base(); ?>group/profile/<?= $group->id ?>" title="View the group profile of <?= $group->name ?>"><?= $group->name ?></a>
			</li>
			<li>
				<span class="label">Created on</span>
				<time datetime="TODO"><?= Helper_Date::niceDate($album->creationDate) ?></time>
			</li>
			<li>
				<span class="label">Description</span>
				<?= $album->description ?>
			</li>
		</ul>
	</div><!-- close column -->

	<div class="twocol last"><!-- open column -->
		<a href="<?= URL::base(); ?>group/profile/<?= $group->id ?>"><img src="<?= $group->getGroupImage('thumb') ?>" title="<?= $group->name ?>" alt="Profile picture of group <?= $group->name ?>"></a>
	</div><!-- close column -->
</div><!-- close row -->

<div class="row">
	<div class="twelvecol">
		<a href="<?= URL::base(); ?>album/addMedia/<?= $album->id ?>">
			<img src="<?= URL::base() ?>assets/img/add.png" title="Add Medias" alt="Add Medias">
		</a>
	</div>
</div>

<div class="row overflow"><!-- open row -->
	<div class="twelvecol"><!-- open column -->
		<div class="gallery"><!-- open #albumGrid -->
			<?php echo View::factory('pages/_photoGallery')->bind('medias', $medias) ?>
		</div><!-- close #albumGrid -->
	</div><!-- close column -->
</div><!-- close row -->

<div class="row "><!-- open row -->
	<div class="twelvecol"><!-- open column -->

	</div><!-- close column -->
</div><!-- close row -->