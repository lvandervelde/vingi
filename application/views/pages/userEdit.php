<div class="row pageTitle"><!-- open row -->
  <div class="twelvecol"><!-- open column -->
    <h1><img src="<?php echo URL::base(); ?>assets/img/modify.png" alt="Edit your profile"> Edit your profile</h1>
  </div><!-- close column -->
</div><!-- close row -->

<div class="row"><!-- open row -->
  <div class="sixcol"><!-- open column -->
    <form id="formUserEdit" action="<?=url::base(); ?>user/changeUser" method="post"><!-- open #formUserEdit -->
      <fieldset>
        <legend>Personal details</legend>

        <div>
          <label for="userEditFirstName">First name</label>
          <input id="userEditFirstName" class="text" name="user[firstname]" type="text" disabled value="<? echo $user->firstname; ?>" placeholder="First name">
        </div>

        <div>
          <label for="userEditLastName">Last name</label>
          <input id="userEditLastName" class="text" name="user[lastname]" type="text" disabled value="<? echo $user->lastname; ?>" placeholder="Last name">
        </div>
      </fieldset>

      <fieldset>
        <legend>Contact details</legend>

        <div>
          <label for="userEditEmail">Email</label>
          <input id="userEditEmail" class="text" name="user[email]" type="email" value="<? echo $user->email; ?>" placeholder="Email address">
        </div>

        <div>
          <label for="userEditPhone">Phone</label>
          <input id="userEditPhone" class="text" name="user[phonenumber]" type="tel" value="<? echo $user->phonenumber; ?>" placeholder="Phone number">
        </div>
      </fieldset>

      <fieldset>
        <legend>Profile picture</legend>

        <div>
          <label for="userEditAvatar">Upload image</label>
          <input id="userEditAvatar" name="user[avatar]" type="file">
        </div>
      </fieldset>

      <input name="userEdit[submit]" type="submit" value="Submit">
    </form><!-- close #formUserEdit -->
  </div><!-- close column -->

  <div class="sixcol last"><!-- open column -->
    <figure id="userAvatarPreview" style="display: none;">
      <img src="TODO" title="Preview your profile image" alt="Profile image preview">
      <figcaption>Profile image preview</figcaption>
    </figure>
  </div><!-- close column -->
</div><!-- close row -->