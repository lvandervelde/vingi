<div class="row pageTitle"><!-- open row -->
  <div class="twelvecol"><!-- open column -->
    <h1>Edit album <?=$album->name?></h1>
  </div><!-- close column -->
</div><!-- close row -->

<div class="row"><!-- open row -->
  <div class="sixcol form"><!-- open column -->
    <form id="formAlbumEdit" action="<?=url::base(); ?>album/changeAlbum" method="post" enctype="multipart/form-data"><!-- open #formAlbumEdit -->
      <fieldset>
        <legend>Album details</legend>

        <div>
          <label for="albumEditName">Name</label>
          <input id="albumEditName" class="text" name="album[name]" type="text" placeholder="Name" value="<?=$album->name?>">
        </div>

        <div>
          <label for="albumEditDescription">Description</label>
          <textarea id="albumEditDescription" rows="10" name="album[description]" placeholder="Description"><?=$album->description?></textarea>
        </div>
      </fieldset>
      
      <fieldset>
        <legend>Album cover image</legend>

        <div>
            <label>Select an image</label>
            <button class="button" type="button">Open filebrowser</button>
            <input type="hidden" name="album[coverId]">
        </div>
        <div>
          <label for="albumEditAvatar">or upload an image</label>
          <input id="albumEditAvatar" name="albumCover" type="file">
        </div>
      </fieldset>

      <input type="hidden" name="album[id]" value="<?=$album->id?>">
      <input name="albumEdit[submit]" type="submit" value="Submit">
    </form><!-- close #formAlbumEdit -->
  </div><!-- close column -->
  
  <div class="sixcol last"><!-- open column -->
    <figure id="groupAvatarPreview" style="display: none;">
      <img src="TODO" title="Preview the album image" alt="Album image preview">
      <figcaption>Album image preview</figcaption>
    </figure>
  </div><!-- close column -->
</div><!-- close row -->
