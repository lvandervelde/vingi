<br>
<div class="row pageTitle">
	<!-- open row -->
	<div class="twelvecol">
		<!-- open column -->
		<h1></h1>
	</div>
	<!-- close column -->
</div>
<!-- close row -->

<div class="row">
	<!-- open row -->
	<div class="eightcol mediaView">
		<!-- open column -->
		<div class="mediaWrapper">
			<img class="media"
					 src="<?php echo $media->getMediaPath('small', $user); ?>"
					 title="<?php echo sprintf('%s: %s', $media->name, $media->description); ?>"
					 alt="<?php echo $media->description; ?>">
			<div class="title"><?php echo $media->name; ?></div>
			<div class="mediaDate"> <?php echo Helper_Date::niceDateTime($media->creationDate); ?></div>
			<img class="userIcon" mediaid="<?= $media->id ?>" src="<?= url::base() ?>assets/img/icon-users.png" title="Set as group image">
			<select id="groupSelect"></select>
			<img class="icon" mediaid="<?= $media->id ?>" src="<?= url::base() ?>assets/img/picture.png" title="Set as album cover">
			<select id="albumSelect"></select>
			<img class="fullscreen" mediaid="<?= $media->id ?>" src="<?= url::base() ?>assets/img/fullscreen.png" title="Go fullscreen">
		</div>

		<?php if ($prevNextMedia): ?>
			<nav id="mediaNav">
				<!-- open #mediaNav -->
				<a href="<?= $prev->getPublicPath() ?>" title="<?= $prev->name ?>">
					<img class="prev" src="<?= url::base() ?>assets/img/arrow-left.png">
				</a>
				<ul>
					<?php foreach ($prevNextMedia as $mediaObj): ?>
						<li>
							<a href="<?= $mediaObj->getPublicPath() ?>" title="<?= $mediaObj->name ?>">
								<img src="<?= $mediaObj->getMediaPath('thumb', $user); ?>" title="<?= $mediaObj->name ?>" alt="<?= $mediaObj->name ?>">
							</a>
							<div class="mediaName">&nbsp;<?= $mediaObj->name ?></div>
						</li>
					<?php endforeach; ?>
				</ul>
				<a href="<?= $next->getPublicPath() ?>" title="<?= $next->name ?>">
					<img class="next" src="<?= url::base() ?>assets/img/arrow-right.png">
				</a>
			</nav>
			<!-- close #mediaNav -->
		<?php endif; ?>

		<div id="comments">
			<!-- open #comments -->
			<h3>Comments</h3>
			<?php echo View::factory('pages/_comment')->bind('comments', $comments); ?>
		</div>
		<!-- close #comments -->

		<?php echo View::factory('pages/_commentCreate')->bind('media', $media); ?>
	</div>
	<!-- close column -->

	<div class="fourcol last">
		<!-- open column -->

		<div class="mediaInfo">
			<ul>
				<?php if ($media->userId == $session->id): ?>
					<a class="edit" href="<?= url::base() ?>media/edit/<?= $media->mediaUnique ?>">
						<img class="icon" src="<?= url::base() ?>assets/img/modify.png" alt="edit" title="Media wijzigen">
					</a>
				<?php endif; ?>

				<li>
					<img class="icon" src="<?= url::base() ?>assets/img/icon-eye.png">
					<?php echo $media->views; ?> views
				</li>
				<li>
					<img class="icon" src="<?= url::base() ?>assets/img/icon-comment.png">
					<?php echo $media->comments->count_all(); ?> comments
				</li>
				<hr>
				<li><img class="userIcon" src="<?= url::base() ?><?= $user->getAvatar($user) ?>">
					<a href="<?php echo sprintf('%suser/profile/%s', url::base(), $user->userUnique); ?>"
						 title="Go to the profile of <?php echo "$user->firstname $user->lastname"; ?>">
							 <?php echo "$user->firstname $user->lastname"; ?>
					</a>
				</li>
			</ul>
		</div>

		<div class="mediaInfo">
			<div class="title">Media info</div>
			<hr>
			<p class="description">
				<?php echo $media->description; ?>
			</p>
			<hr>
			<div id="mediaTagCloud" class="wordCloud" media="<?= $media->id ?>"></div>
		</div>

		<div class="mediaInfo">
			<div class="title">Media links</div>
			<hr>
			<table>
				<tr>
					<th><label for="email">Email</label></th>
					<td><input type="text" name="email" onclick="select()" value="<?= $_SERVER['HTTP_HOST'].$media->getPublicPath() ?>"></td>
				</tr>
				<tr>
					<th><label for="img">IMG code</label></th>
					<td><input type="text" name="img" onclick="select()" value="[IMG]<?= $_SERVER['HTTP_HOST'].$media->getMediaPath() ?>[/IMG]"></td>
				</tr>
				<tr>
					<th><label for="link">Direct link</label></th>
					<td><input type="text" name="link" onclick="select()" value="<?= $_SERVER['HTTP_HOST'].$media->getMediaPath() ?>"></td>
				</tr>
			</table>
		</div>
	</div>
	<!-- close column -->
</div>
<!-- close row -->
