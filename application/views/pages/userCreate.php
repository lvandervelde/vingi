<div class="row pageTitle"><!-- open row -->
  <div class="twelvecol"><!-- open column -->
    <h1>Register your group</h1>
  </div><!-- close column -->
</div><!-- close row -->

<div class="row form"><!-- open row -->
  <div class="twelvecol"><!-- open column -->
    <form id="formRegister" action="<?php echo url::base(); ?>login/registerUser" method="post" enctype="multipart/form-data"><!-- open #formRegister -->
      <fieldset>
        <legend>Personal details</legend>
        <div>
          <label for="registerFirstName">First name</label>
          <input id="registerFirstName" class="text" name="user[firstname]" type="text" placeholder="First name">
        </div>

        <div>
          <label for="registerLastName">Last name</label>
          <input id="registerLastName" class="text" name="user[lastname]" type="text" placeholder="Last name">
        </div>
        
        <div>
          <label for="registerGender">Gender</label>
          <input type="Radio" placeholder="Gender" name ="user[gender]" value= '0' checked>Male
		  <input type="Radio" placeholder="Gender" name ="user[gender]" value= '1'>Female
        </div>
      </fieldset>

      <fieldset>
        <legend>Contact details</legend>

        <div>
          <label for="registerEmail">Email</label>
          <input id="registerEmail" class="text" name="user[email]" type="email" placeholder="Email">
        </div>

        <div>
          <label for="registerPhone">Phone</label>
          <input id="registerPhone" class="text" name="user[phoneNumber]" type="tel" placeholder="Phone number">
        </div>
      </fieldset>

      <fieldset>
        <legend>Group details</legend>

        <div>
          <label for="registerGroupName">Group name</label>
          <input id="registerGroupName" class="text" name="group[name]" type="text" placeholder="Group name">
        </div>

        <div>
          <label for="registerDescription">Group description</label>
          <textarea id="registerDescription" name="group[description]" placeholder="Description"></textarea>
        </div>
        
        <div>
          <label for="registerImage">Group image</label>
          <input id="groupImage" name="groupImage" type="file">
        </div>
      </fieldset>

      <fieldset>
        <legend>Account details</legend>

        <div>
          <label for="registerPassword">Password</label>
          <input id="registerPassword" class="text" name="user[password]" type="password" placeholder="Password">
        </div>
        
        <div>
          <label for="registerPassword2">Verify password</label>
          <input id="registerPassword2" class="text" name="user[password2]" type="password" placeholder="Password (repeat)">
        </div>
      </fieldset>

      <input name="register[submit]" type="submit" value="Register">
    </form><!-- close #formRegister -->
  </div><!-- close column -->
</div><!-- close row -->