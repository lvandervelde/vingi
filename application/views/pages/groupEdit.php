<div class="row pageTitle"><!-- open row -->
  <div class="twelvecol"><!-- open column -->
    <h1><img src="<?php echo URL::base(); ?>assets/img/modify.png" alt="Edit this group"> <? echo $group->name; ?></h1>
  </div><!-- close column -->
</div><!-- close row -->

<div class="row"><!-- open row -->
  <div class="sixcol"><!-- open column -->
    <form id="formGroupEdit" action="<?= url::base(); ?>group/changeGroup" method="post"><!-- open #formGroupEdit -->
      <fieldset>
        <legend>Group details</legend>

        <div>
          <label for="groupEditName">Name</label>
          <input id="groupEditName" class="text" name="group[name]" type="text" placeholder="Name" value="<? echo $group->name; ?>">
        </div>

        <div>
          <label for="groupEditDescription">Description</label>
          <textarea id="groupEditDescription" name="group[description]" placeholder="Description"><? echo $group->description; ?></textarea>
        </div>
      </fieldset>
      <input type="hidden" name="group[id]" value="<?=$group->id?>">
      <input name="group[submit]" type="submit" value="Submit">
    </form><!-- close #formGroupEdit -->
  </div><!-- close column -->

</div><!-- close row -->
