
<div class="row pageTitle">
	<!-- open row -->
	<div class="twelvecol">
		<!-- open column -->

		<div id="headerMediaUploader">
			<h2>Upload new media</h2>


			<div id="uploadButtons" class="filter">
				<button id="addPhoto">Add</button>
				<button id="removePhoto">Remove</button>
			</div>

			<div id="groupsWrapper">
				<?php
				if (count($groups)> 1) {
					echo 'Group: <select id="groupIdVal">';

					foreach ($groups as $group) {
						echo '<option value="' . $group->id . '">' . $group->name . '</option>';
					}

					echo '</select>';
				} else {
                    echo '<span>' . $groups[0]->name . '</span><input type="hidden" id="groupIdVal" value="' . $groups[0]->id . '"> ';
                    if(isset($album)){
						echo '<span>&raquo;&nbsp;' . $album->name . '</span><input type="hidden" id="albumIdVal" value="' . $album->id . '"> ';
					}
                }
                ?>
			</div>
		</div>

	</div>
	<!-- close column -->
</div>
<!-- close row -->

<div class="row">
	<!-- open row -->
	<div class="twelvecol">
		<!-- open column -->

		<div id="mediaUploader">
			<!-- open #mediaUploader -->

			<div id="mediaUploaderText">
				<!-- open #mediaUploaderText -->
				<p>Drag files here</p>
				<p>You can also...</p>
				<button>Select files from your computer</button>
				<br>
				<form id="fileUploadForm" name="fileUploadForm"
					action="http://vingi.nl/media/uploadProgress" method="post"
					enctype="multipart/form-data">
					<input type="file" id="fileInput" size="60" name="files[]" multiple>
					<input type="hidden" id="imagesOriginal" name="imagesOriginal"> <input
						type="hidden" id="imagesNames" name="imagesNames">
				</form>
			</div>
			<!-- close #mediaUploaderText -->

		</div>
		<!-- close #mediaUploader -->

		<div id="mediaUploaderButtons">
			<!-- open #mediaUploaderButtons -->
			<button id="ok">Ok</button>
			<button id="cancel">Cancel</button>
		</div>
		<!-- close #mediaUploaderButtons -->

	</div>
	<!-- close column -->
</div>
<!-- close row -->
