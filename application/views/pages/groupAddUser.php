<div class="row pageTitle"><!-- open row -->
  <div class="twelvecol"><!-- open column -->
    <h1>Join GROUP NAME</h1>
  </div><!-- close column -->
</div><!-- close row -->

<div class="row"><!-- open row -->
  <form>
    <div class="twelvecol"><!-- open column -->
      <div>
        <label for="accountNew">I don't have an account</label>
        <input id="accountNew" name="account" type="radio" value="0" checked="checked">
      </div>

      <div>
        <label for="accountExisting">I already have an account</label>
        <input id="accountExisting" name="account" type="radio" value="1">
      </div>
    </div><!-- close column -->
  </form>
</div><!-- close row -->

<div class="row"><!-- open row -->
  <div class="twelvecol"><!-- open column -->
    <form id="formRegister" action="#" method="post"><!-- open #formRegister -->
      <fieldset>
        <legend>Personal details</legend>
        <div>
          <label for="registerFirstName">First name</label>
          <input id="registerFirstName" class="text" name="user[firstname]" type="text" placeholder="First name">
        </div>

        <div>
          <label for="registerLastName">Last name</label>
          <input id="registerLastName" class="text" name="user[lastname]" type="text" placeholder="Last name">
        </div>
      </fieldset>

      <fieldset>
        <legend>Contact details</legend>

        <div>
          <label for="registerEmail">Email</label>
          <input id="registerEmail" class="text" name="user[email]" type="email" placeholder="Email">
        </div>

        <div>
          <label for="registerPhone">Phone</label>
          <input id="registerPhone" class="text" name="user[phoneNumber]" type="tel" placeholder="Phone number">
        </div>
      </fieldset>

      <fieldset>
        <legend>Account details</legend>

        <div>
          <label for="registerPassword">Password</label>
          <input id="registerPassword" class="text" name="user[password]" type="password" placeholder="Password">
        </div>

        <div>
          <label for="registerPassword2">Verify password</label>
          <input id="registerPassword2" class="text" name="user[password2]" type="password" placeholder="Password (repeat)">
        </div>
      </fieldset>

      <input name="register[submit]" type="submit" value="Join">
    </form><!-- close #formRegister -->

    <form id="formLogin" style="display: none;" action="#" method="post"><!-- open #formLogin -->
      <fieldset>
        <legend>Login</legend>

        <div>
          <label for="loginEmail">Email</label>
          <input id="loginEmail" class="text" name="login[email]" type="text" placeholder="Email">
        </div>

        <div>
          <label for="loginPassword">Password</label>
          <input id="loginPassword" class="text" name="login[password]" type="password" placeholder="Password">
        </div>

        <input name="login[submit]" type="submit" value="Join">
      </fieldset>
    </form><!-- close #formLogin -->
  </div><!-- close column -->
</div><!-- close row -->