<div class="row pageTitle"><!-- open row -->
  <div class="twelvecol"><!-- open column -->
    <h1>Create new album</h1>
  </div><!-- close column -->
</div><!-- close row -->

<div class="row"><!-- open row -->
  <div class="twelvecol form"><!-- open column -->
    <form id="formAlbumCreate" action="<?=url::base(); ?>album/newAlbum" method="post" enctype="multipart/form-data"><!-- open #formAlbumCreate -->
      <fieldset>
        <legend>Group details</legend>

        <div>
          <label for="albumCreateName">Name</label>
          <input id="albumCreateName" class="text" name="album[name]" type="text" placeholder="Name">
        </div>

        <div>
          <label for="albumCreateDescription">Description</label>
          <textarea id="albumCreateDescription" rows="10" name="album[description]" placeholder="Description"></textarea>
        </div>
      </fieldset>
        
      <fieldset>
        <legend>Album cover image</legend>

        <div>
          <label for="albumEditAvatar">Upload image</label>
          <input id="albumEditAvatar" name="albumCover" type="file">
        </div>
      </fieldset>

      <input type="hidden" name="album[groupId]" value=<?= $group->id ?>>
      <input name="albumCreate[submit]" type="submit" value="Create">
    </form><!-- close #formAlbumCreate -->
  </div><!-- close column -->
</div><!-- close row -->
