<div class="row pageTitle"><!-- open row -->
    <div class="threecol groupItem"><!-- open column -->
        <img id="userImage" src="<?php echo URL::base(); ?><?= $user->getAvatar() ?>" title="<?= $user->firstname ?> <?= $user->lastname ?>" alt="Profile picture of user <?= $user->firstname ?> <?= $user->lastname ?>">
    </div><!-- close column -->

    <div class="eightcol last groupItem overflow">
        <div id="profileHeader"><!-- open #profileHeader -->
            <h1><?= $user->firstname ?> <?= $user->lastname ?>
            <?php if( $edit ):?>
                <a href="<?=URL::base(); ?>user/edit/"><img src="<?php echo URL::base(); ?>assets/img/modify.png"  alt="Edit your profile"></a>
            <?php endif;?>
            </h1>
            <ul>
                <li>
                    <span class="label">Gender</span>
                    <?= $user->getGender() ?>
                </li>
                <li>
                    <span class="label">Phone</span>
                    <?= $user->phonenumber ?>
                </li>
                <li>
                    <span class="label">Email</span>
                    <?= $user->email ?>
                </li>
                <li>
                    <span class="label">Member since</span>
                    <time datetime="TODO"><?= Helper_Date::niceDate($user->registerDate) ?></time>
                </li>
            </ul>
            <br>
            <?php if(isset($invites)): ?>
				<?php if(count($invites)> 0): ?>
					<h2>Invites</h2>
		       		<div id="invites" class="section">
						<ul class="polaroids">
			            	<?php foreach ($invites as $invite): ?>
		                	<li>
		                		<a href="<?php echo URL::base(); ?>invite/view/<?= $invite->id ?>" title="View the invite of <?= $invite->group->name ?>">
		                    		<img src="<?= $invite->group->getGroupImage('thumb') ?>" title="<?= $invite->text ?>" width=128px height=128px alt="<?= $invite->text ?>">
		                		</a>
							</li>
		                	<?php endforeach; ?>
		                </ul>
				    </div>
				<?php endif;?>
			<?php endif;?>
        </div>
    </div>
</div><!-- close row -->

<?php if(count($groups)> 0): ?>
<div class="row overflow"><!-- open row -->
	<div class="twelvecol overflow">
	<h2>Groups</h2>
		<div id="groups" class="section">
			<ul class="polaroids">
	            <?php foreach ($groups as $group): ?>
	            <li>
	                <a href="<?php echo URL::base(); ?>group/profile/<?= $group->id ?>" title="<?= $group->name ?>">
	                    <img src="<?= $group->getGroupImage('thumb') ?>" title="<?= $group->name ?>" alt="<?= $group->name ?>">
	                </a>
				</li>
	            <?php endforeach; ?>
			</ul>
		</div>
	</div>
</div>
<?php endif;?>

<?php if(count($uploads)> 0): ?>
<div class="row overflow"><!-- open row -->
    <div class="twelvecol overflow"><!-- open row -->
        <div id="latestUploads" class="gallery">
            <h2>Latest uploads</h2>
            <?php echo View::factory('pages/_photoGallery')->bind('medias', $uploads) ?><br>
        </div>
    </div>
</div>
<?php endif;?>

<div class="row"><!-- open row -->
    <div class="twelvecol"><!-- open column -->
        <!-- To fix the overlow hidden problem with the previous row -->
    </div><!-- close column -->
</div><!-- close row -->
