<div class="row "><!-- open row -->
    <div class="twelvecol"><!-- open column -->
        <h2>Add Medias</h2>
    </div><!-- close column -->
</div><!-- close row -->

<div class="row">
	<div class="fivecol">
		<a href="<?= URL::base(); ?>album/view/<?=$album->id ?>">
			<h3><?=$album->name ?></h3>
		</a>
	</div>
	<div class="twocol">
		<h3></h3>
	</div>
	<div class="fivecol last">
		<a href="<?= URL::base(); ?>group/profile/<?=$group->id ?>">
			<h3><?=$group->name ?></h3>
		</a>
	</div>
</div>

<div class="row overflow"><!-- open row -->
    <div class="fivecol scrollable"><!-- open column -->
        <div id="albumGrid" class="gallery noDetail"><!-- open #albumGrid -->
            <?php echo View::factory('pages/_photoGallery')->bind('medias', $medias) ?>
        </div><!-- close #albumGrid -->
    </div><!-- close column -->
    <div class="twocol"><!-- open column -->
    	<div class="albumAdd">
    	    <button id="addToAlbum" class="button">&lt;&lt;</button>
    		<button id="removeFromAlbum" class="button">&gt;&gt;</button>
    	</div>
    </div><!-- close column -->
   	<div class="fivecol scrollable last"><!-- open column -->
   		<div id="groupGrid" class="gallery noDetail"><!-- open #albumGrid -->
            <?php echo View::factory('pages/_photoGallery')->bind('medias', $groupMedias) ?>
        </div><!-- close #albumGrid -->
   	</div><!-- close column -->
</div><!-- close row -->

<div class="row "><!-- open row -->
    <div class="twelvecol"><!-- open column -->
        
    </div><!-- close column -->
</div><!-- close row -->