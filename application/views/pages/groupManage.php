
<div class="row pageTitle"><!-- open row -->
    <div class="twocol"><!-- open column -->
        <img src="<?= $group->getGroupImage('thumb') ?>" title="<?= $group->name ?>" alt="Picture of group <?= $group->name ?>">
    </div><!-- close column -->

    <div class="fourcol"><!-- open column -->
        <h1><?= $group->name ?></h1>
        <ul>
            <li>
                <span class="label">Created on</span>
                <time datetime="TODO"><?= $owner->registerDate ?></time>
            </li>
             <li>
                <span class="label">Owner</span>
                <a href="<?php echo url::base(); ?>user/profile/<?=$owner->userUnique ?>" title="Go to the profile of <?=$owner->firstname ?> <?=$owner->lastname ?>">
               		 <?=$owner->firstname ?> <?=$owner->lastname ?>
				</a>
            </li>
        </ul>
    </div><!-- close column -->

    <div class="sixcol last"><!-- open column -->
        <p>
            <?= $group->description ?>
        </p>
    </div><!-- close column -->
</div><!-- close row -->


<div class="row"><!-- open row -->
    <div class="gallery"><!-- open column -->
        <h2>Members
        	<a href="<?=URL::base(); ?>group/profile/<?= $group->id ?>"><img src="<?php echo URL::base(); ?>assets/img/cancel.png" alt="Return to this group"></a>
        </h2>

		<?php foreach($users as $user): ?>
		 <ul>
            <li>
	        	<img src="<?php echo url::base(); ?><?= $user->getAvatar($user) ?>" title="<?=$user->firstname?> <?=$user->lastname?>" class="avatar" alt="Profile picture of user <?=$user->firstname?>">
	       	</li>
	       	<li>
				<span class="label">Name: </span>
	       		<?=$user->firstname ?> <?=$user->lastname ?>
	       	</li>

            <li>
				<form id="formRemove" action="<?= url::base(); ?>group/removeUser/" method="post"> <!-- open #formLogin -->
					<input type="hidden" name="remove[userId]" value="<?=$user->id?>">
	        		<input type="hidden" name="remove[groupId]" value="<?=$group->id?>">
					<input type="submit" name="remove[submit]" value="Remove">
	        	</form> <!-- close # -->
	        </li>
		</ul>
		<?php endforeach; ?>
    </div><!-- close column -->
</div><!-- close row -->
