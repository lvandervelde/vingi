<div class="row pageTitle"><!-- open row -->
	<div class="groupItem twocol"><!-- open column -->
		<?php if ($edit): ?>
			<a href="<?= URL::base(); ?>group/editGroupImage/<?= $group->id ?>"><img src="<?= $group->getGroupImage('thumb') ?>" title="<?= $group->name ?>" alt="Edit image of group <?= $group->name ?>"></a>
		<?php else: ?>
			<img src="<?= $group->getGroupImage('thumb') ?>" title="<?= $group->name ?>" alt="Picture of group <?= $group->name ?>">
		<?php endif; ?>
	</div><!-- close column -->

	<div class="groupItem ninecol"><!-- open column -->
		<h1><?= $group->name ?>
			<?php if ($edit): ?>
				<a href="<?= URL::base(); ?>group/edit/<?= $group->id ?>">
					<img src="<?php echo URL::base(); ?>assets/img/modify.png" alt="Edit this group">
				</a>
			<?php endif; ?>
		</h1>
		<ul>
			<li>
				<span class="label">Created on</span>
				<time datetime="TODO"><?= Helper_Date::niceDate($owner->registerDate) ?></time>
			</li>
			<li>
				<span class="label">Owner</span>
				<a href="<?php echo url::base(); ?>user/profile/<?= $owner->userUnique ?>" title="Go to the profile of <?= $owner->firstname ?> <?= $owner->lastname ?>">
					<?= $owner->firstname ?> <?= $owner->lastname ?>
				</a>
				<br>
			</li>
			<li>
				<br>
				<span class="label">Description</span>
				<?= $group->description ?>
			</li>
		</ul>
	</div><!-- close column -->

	<div class="row overflow"><!-- open row -->
    <div class="twelvecol">
			<div class="section">
				<h2>Albums
					<?php if ($addAlbum): ?>
						<a href="<?php echo URL::base(); ?>album/create/<?= $group->id ?>"><img src="<?php echo URL::base(); ?>assets/img/add.png" alt="Create a new album"></a>
					<?php endif; ?>
				</h2>
				<ul class="polaroids">
					<?php foreach ($albums as $album): ?>
						<li>
							<a href="<?= url::base() ?>album/view/<?= $album->id ?>" title="<?= $album->name ?>">
								<img src="<?= $album->getAlbumImage('thumb') ?>" alt="<?= $album->name ?>">
							</a>
							<br>
							<img class="icon" src="<?= url::base() ?>assets/img/icon-photo.gif"> <?= $album->getMediaCount() ?> photos
						</li>
					<?php endforeach; ?>
				</ul>
			</div>
    </div>
	</div><!-- close row -->

	<div class="row overflow"><!-- open row -->
    <div class="twelvecol"><!-- open column -->
			<div class="section">
				<div id="latestUploads" class="gallery">
					<h2>Latest uploads
						<a href="<?php echo URL::base(); ?>media/upload/<?= $group->getGroupNameEncode() ?>">
							<img src="<?php echo URL::base(); ?>assets/img/upload.png" alt="Upload new media">
						</a>
					</h2>
					<?php echo View::factory('pages/_photoGallery')->bind('medias', $uploads) ?>
				</div>
			</div>
    </div><!-- close column -->
	</div><!-- close row -->


	<div class="row"><!-- open row -->
    <div class="twelvecol">
			<div class="section">
				<h2>Members
					<?php if ($invite): ?>
						<a href="<?php echo URL::base(); ?>group/inviteUser/<?= $group->id ?>">
							<img src="<?php echo URL::base(); ?>assets/img/invite.png">
						</a>
					<?php endif; ?>
					<?php if (count($users) > 0 && $invite): ?>
						<a href="<?php echo URL::base(); ?>group/manage/<?= $group->id ?>">
							<img src="<?php echo URL::base(); ?>assets/img/modify.png">
						</a>
					<?php endif; ?>
				</h2>
				<?php echo View::factory('pages/_userGallery')->bind('users', $users) ?>

			</div><!-- close column -->
		</div><!-- close column -->
	</div><!-- close row -->
</div>
