
<?php foreach($albums as $album): ?>

<div class="outerWrapper">
    <img id="<?=$album->coverImage ?>" userid="<?=$album->user->userUnique ?>" src="<?=$album->getMediaPath('thumb', $album->user); ?>" width="128" height="128" title="<?=$album->name?>" alt="<?=$album->name?>">
    
    <div style="display: none;" id="img_<?=$album->id; ?>_details" class="imageDetails">
        
        <div class="contentWrapper">
            <div class="imageName"><?=$album->name?>&nbsp;</div>
            
            <div class="imageWrapper">
                <a href="<?php echo url::base(); ?>media/view/<?=$album->id ?>"><img src="<?=$album->getMediaPath('thumb', $album->user); ?>" width="128" height="128" title="<?=$album->name; ?>" alt="<?=$album->name; ?>"></a>
            </div> 

            <dl>
                <dd>
                    <ul>
                        <li>
                            <span class="label">By</span>
                            <a href="<?php echo url::base(); ?>user/profile/<?=$album->user->userUnique ?>" title="Go to the profile of <?=$album->user->firstname ?> <?=$album->user->lastname ?>">
                                <?=$album->user->firstname ?> <?=$album->user->lastname ?>
                            </a>
                        </li>
                        <li>
                            <span class="label">In</span>
                            <a href="<?=url::base() ?>album/view/<?=$album->getAlbum()->id ?>" title="Go to the album <?=$album->getAlbum()->name ?>">
                                <?=$album->getAlbum()->name ?>
                            </a>
                        </li>
                    </ul>
                </dd>
            </dl>
        </div>
        
    </div>
</div>
    
<?php endforeach; ?>
