<div class="row pageTitle">
	<!-- open row -->
	<div class="twelvecol">
		<!-- open column -->
		<h1>
			<img src="<?php echo URL::base(); ?>assets/img/remove.png" alt="Remove this user!" > Remove "<?= $user->firstname?> <?= $user->lastname?>" from "<?= $group->name?>"!
		</h1>
	</div>
	<!-- close column -->
</div>
<!-- close row -->

<div class="row"> <!-- open row -->
	<div class="twelvecol"> <!-- open column -->
		<form id="formInvite" action="<?= url::base(); ?>group/deleteUser" method="post"> <!-- open #formLogin -->
			<fieldset>
				<legend>Remove</legend>

				<div>
					<label for="userLabel">User</label>
					<label for="userName"><?= $user->firstname?> <?= $user->lastname?> </label>
				</div>
				<div>
					<label for="userImage">Image</label>
					<img src="<?php echo url::base(); ?><?= $user->getAvatar($user) ?>" title="<?= $user->firstname?> <?= $user->lastname?>" class="avatar" alt="<?= $user->firstname?> <?= $user->lastname?>">
				</div>
				<div>
					<label for="userMessage">Remove</label>
					<label for="userDescription">Do you really want to remove <?= $user->firstname?> <?= $user->lastname?> from <?= $group->name?>?</label>
				</div>
				<input type="hidden" name="delete[userId]" value="<?=$user->id?>">
				<input type="hidden" name="delete[groupId]" value="<?=$group->id?>">
				<input type="submit" name="delete[submit]" value="Yes" >
				<input type="submit" name="delete[submit]" value="No" >

			</fieldset>
		</form> <!-- close #formLogin -->
	</div> <!-- close column -->
</div> <!-- close row -->
