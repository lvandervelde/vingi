<?php foreach ($comments as $comment): ?>
	<article>
		<h3><?php //echo Text::limit_words($comment->text, 100, '&hellip;');  ?></h3>

		<div class="meta">
			<img class="avatar" src="<?= url::base() ?>assets/img/avatar.jpg" width="28" height="32" alt="Profile picture">

			<?php if ($comment->isPublisher()): ?>
				<img class="modify" src="<?= url::base() ?>assets/img/modify.png" width="14" height="14">
			<?php endif; ?>

			<p class="text">
				<a href="<?= url::base() ?>user/profile/<?= $comment->user->userUnique ?>" title="Go to the profile of <?= $comment->user->firstname ?> <?= $comment->user->lastname ?>">
					<?= $comment->user->firstname ?> <?= $comment->user->lastname ?>
				</a>
				<br>
				<time datetime="<?= Helper_Date::niceDateTime($comment->creationDate); ?>" pubdate="pubdate">
					<?= Helper_Date::niceDateTime($comment->creationDate); ?>
				</time>
			</p>

		</div>

		<div id="<?= $comment->id ?>" class="body">
			<p class="text">
				<?php echo $comment->text; ?>
			</p>
			<time class="editdate">Last edit: <?= Helper_Date::niceDateTime($comment->lastEditDate) ?></time>
		</div>

	</article>
<?php endforeach; ?>