
<?php foreach($medias as $media): ?>

<div class="outerWrapper hidden">
    <img id="<?=$media->mediaUnique; ?>" mediaid="<?=$media->id ?>" userid="<?=$media->user->userUnique ?>" src="<?=$media->getMediaPath('thumb', $media->user); ?>" width="128" height="128" title="<?=$media->name?>" alt="<?=$media->name?>">

    <div style="display: none;" id="img_<?=$media->id; ?>_details" class="imageDetails">

        <div class="contentWrapper">
            <div class="imageName"><?=$media->name?>&nbsp;</div>

            <div class="imageWrapper">
                <a href="<?=$media->getPublicPath() ?>"><img src="<?=$media->getMediaPath('thumb', $media->user); ?>" width="128" height="128" title="<?=$media->name; ?>" alt="<?=$media->name; ?>"></a>
            </div>

            <dl>
                <dd>
                    <table>
                        <tr>
                            <td><img class="userIcon" src="<?=url::base()?>assets/img/icon-user.png"></td>
                            <td>
                                <a class="userName" href="<?php echo url::base(); ?>user/profile/<?=$media->user->userUnique ?>" title="Go to the profile of <?=$media->user->firstname ?> <?=$media->user->lastname ?>">
                                    <?=$media->user->firstname ?> <?=$media->user->lastname ?>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td><img class="icon" src="<?=url::base()?>assets/img/icon-eye.png"></td>
                            <td><span><?=$media->views ?> views<span></td>
                        </tr>
                        <tr>
                            <td><img class="icon" src="<?=url::base()?>assets/img/icon-comment.png"></td>
                            <td><span><?=$media->comments->count_all() ?> comments<span></td>
                        </tr>
                      	<tr>
                            <td colspan="2"><?=Helper_Date::niceDateTime($media->creationDate) ?></td>
                        </tr>
                    </table>
                </dd>
            </dl>
        </div>

    </div>
</div>

<?php endforeach; ?>
