<div class="row groups">
	<h1>Groups</h1>
	<?php foreach($groups as $group): ?>


	<div class="row overflow">
		<!-- open row -->
		<div class="twelvecol groupItem overflow">
			<!-- open column -->
			<div class="section">
				<div class="twocol">
					<!-- open column -->
					<a href="<?php echo url::base(); ?>group/profile/<?=$group->id ?>">
						<img src="<?= $group->getGroupImage('thumb') ?>"
						title="<?= $group->name ?>"
						alt="Picture of group <?= $group->name ?>">
					</a>
				</div>
				<!-- close column -->
				<div class="fourcol">
					<!-- open column -->
					<a class="groupName" href="<?php echo url::base(); ?>group/profile/<?=$group->id ?>"><h1>
							<?= $group->name ?>
						</h1>
					</a>
					<ul>
						<li><span class="label">Created on</span> <time datetime="TODO">
								<?= Helper_Date::niceDate($group->getGroupOwner()->registerDate) ?>
							</time></li>
						<li><span class="label">Owner</span> <a
							href="<?php echo url::base(); ?>user/profile/<?=$group->getGroupOwner()->userUnique ?>"
							title="Go to the profile of <?=$group->getGroupOwner()->firstname ?> <?=$group->getGroupOwner()->lastname ?>">
								<?=$group->getGroupOwner()->firstname ?> <?=$group->getGroupOwner()->lastname ?>
						</a></li>
                                                <?php if($group->ownerId != $user->id): ?>
                                                    <li><br><a href="<?=URL::base()?>user/leaveGroup/<?=$group->id ?>">Leave group</a></li>
                                                <?php endif; ?>
					</ul>
				</div>
				<!-- close column -->
				<div id="latestUploads" class="sixcol last gallery">
					<?php echo View::factory('pages/_photoGallery')->bind('medias', $group->getMedias(3, 0)) ?>
				</div>
			</div>
		</div>
		<!-- close column -->
	</div>
	<!-- close row -->
	<?php endforeach; ?>
</div>
