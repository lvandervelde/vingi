<div class="row pageTitle"><!-- open row -->
  <div class="twelvecol"><!-- open column -->
    <h1>Create new group</h1>
  </div><!-- close column -->
</div><!-- close row -->

<div class="row"><!-- open row -->
  <div class="twelvecol"><!-- open column -->
    <form id="formGroupCreate" action="#" method="post"><!-- open #formGroupCreate -->
      <fieldset>
        <legend>Group details</legend>

        <div>
          <label for="groupCreateName">Name</label>
          <input id="groupCreateName" class="text" name="group[name]" type="text" placeholder="Name">
        </div>

        <div>
          <label for="groupCreateDescription">Description</label>
          <textarea id="groupCreateDescription" name="group[description]" placeholder="Description"></textarea>
        </div>
      </fieldset>

      <input name="groupCreate[submit]" type="submit" value="Create">
    </form><!-- close #formGroupCreate -->
  </div><!-- close column -->
</div><!-- close row -->