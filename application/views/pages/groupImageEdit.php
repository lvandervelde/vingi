<div class="row pageTitle">
	<!-- open row -->
	<div class="twelvecol">
		<!-- open column -->
		<h1>
			<img src="<?php echo URL::base(); ?>assets/img/modify.png" alt="Edit this group">
			<? echo $group->name; ?>
		</h1>
	</div>
	<!-- close column -->
</div>
<!-- close row -->

<div
	class="row">
	<!-- open row -->
	<div class="sixcol">
		<!-- open column -->
		<form id="formGroupEdit" action="<?= url::base(); ?>group/changeGroupImage" method="post" enctype="multipart/form-data">
			<!-- open #formGroupEdit -->
			<fieldset>
				<legend>Group picture</legend>
				<img src=<?= $group->getGroupImage('thumb') ?>>
				<div>
					<label for="groupEditImage">Upload image</label>
					<input id="groupEditImage" name="groupImage" type="file">
				</div>
			</fieldset>
			<input type="hidden" name="group[id]" value="<?=$group->id?>">
			<input name="group[submit]" type="submit" value="Submit">


		</form>
		<!-- close #formGroupEdit -->
	</div>
	<!-- close column -->

	<div class="sixcol last">
		<!-- open column -->
		<figure id="groupAvatarPreview" style="display: none;">
			<img src="TODO" title="Preview the group image"
				alt="Group image preview">
			<figcaption>Group image preview</figcaption>
		</figure>
	</div>
	<!-- close column -->
</div>
<!-- close row -->
