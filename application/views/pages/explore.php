<div class="row">
    <div class="sevencol">

        <div class="section">
            <div class="title">Popular groups</div>

                <ul class="polaroids">
                <?php foreach($groups as $group): ?>
                    <li>
                        <a href="<?php echo $group->hasUser($user->id) ? url::base()."group/profile/".$group->id : '#'; ?>" title="<?=$group->name ?>">
                            <img src="<?=$group->getGroupImage('thumb') ?>" alt="<?=$group->name ?>">
                        </a>
                        <br>
                        <img class="usericon" src="<?=url::base()?>assets/img/icon-users.png"> <span><?=$group->members ?> members<span>
                    </li>
                <?php endforeach; ?>
                </ul>
        </div>

    </div>

    <div class="fivecol last">
        <div id="tagcloud" class="section">

            <div id="wordcloud"></div>

        </div>
    </div>
</div>

<div class="row">
    <div class="twelvecol">
        <div class="section">
            <div class="title">Popular albums</div>

                <ul class="polaroids">
                <?php foreach($albums as $album): ?>
                    <li>
                        <a href="<?=url::base() ?>album/view/<?=$album->id ?>" title="<?=$album->name ?>">
                            <img src="<?=$album->getAlbumImage('thumb') ?>" alt="<?=$album->name ?>">
                        </a>
                        <br>
                        <img class="icon" src="<?=url::base()?>assets/img/icon-photo.gif"> <span><?=$album->media ?> photos<span>
                    </li>
                <?php endforeach; ?>
                </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="twelvecol">
        <div class="section">
            <div class="title">Popular photos</div>

                <ul class="polaroids">
                <?php foreach($medias as $media): ?>
                    <li>
                        <a href="<?=$media->getPublicPath() ?>" title="<?=$media->name ?>">
                            <img src="<?=$media->getMediaPath('thumb') ?>" alt="<?=$media->name ?>">
                        </a>
                        <br>
                        <img class="icon" src="<?=url::base()?>assets/img/icon-eye.png"> <span><?=$media->views ?> views<span>
                    </li>
                <?php endforeach; ?>
                </ul>
        </div>
    </div>
</div>



