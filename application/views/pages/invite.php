<div class="row pageTitle">
	<!-- open row -->
	<div class="twelvecol">
		<!-- open column -->
		<h1>
			<img src="<?php echo URL::base(); ?>assets/img/invite.png" alt="You are invited!"> You are invited for "<?= $invite->group->name?>"!
		</h1>
	</div>
	<!-- close column -->
</div>
<!-- close row -->

<div class="row"> <!-- open row -->
	<div class="twelvecol"> <!-- open column -->
		<form id="formInvite" action="<?= url::base(); ?>invite/response" method="post"> <!-- open #formLogin -->
			<fieldset>
				<legend>Invitation</legend>

				<div>
					<label for="inviteLabel">Group</label>
					<label for="inviteGroup"><?= $invite->group->name?> </label>
				</div>
				<div>
					<label for="inviteImage">Image</label>
					<img src="<?= $invite->group->getGroupImage('thumb') ?>" title="<?= $invite->text ?>" alt="<?= $invite->text ?>">
				</div>
				<div>
					<label for="inviteInvitation">Invitation</label>
					<label for="inviteDescription"><?= $invite->text?> </label>
				</div>

				<input type="hidden" name="invite[email]" value="<?=$invite->email?>">
				<input type="hidden" name="invite[groupId]" value="<?=$invite->groupId?>">
				<input type="submit" name="invite[submit]" value="accept">
				<input type="submit" name="invite[submit]" value="decline">
				<input type="submit" name="invite[submit]" value="cancel">

			</fieldset>
		</form> <!-- close #formLogin -->
	</div> <!-- close column -->
</div> <!-- close row -->
