
<div class="groupMembers">
<?php foreach($users as $user): ?>

    <a href="<?php echo url::base(); ?>user/profile/<?=$user->userUnique ?>" title="Go to the profile of <?=$user->firstname?>" class="avatar">
        <img src="<?php echo url::base(); ?><?= $user->getAvatar() ?>" title="<?=$user->firstname?> <?=$user->lastname?>" alt="Profile picture of user <?=$user->firstname."".$user->lastname?> ">
    </a>
    
<?php endforeach; ?>
</div>
