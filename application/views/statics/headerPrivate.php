<div class="row"><!-- open row -->
  <div class="twocol"><!-- open column -->
    <a href="<?php echo URL::base(); ?>home" title="Return to Vingi Live Stream">
      <img src="<?php echo URL::base(); ?>assets/img/logo_light.png" title="Return to Vingi Live Stream" alt="Return to Vingi Live Stream">
    </a>
  </div><!-- close .twocol -->
  <div class="ninecol"><!-- open .twelvecol -->
    <nav id="mainNav"><!-- open #mainNav -->
      <ul>
        <li><a <?=(($active === 'home') ? ' class="active"' : NULL)?> href="<?php echo URL::base(); ?>home" title="Return to Vingi Live Stream">Live stream</a></li>
        <li><a <?=(($active === 'explore') ? ' class="active"' : NULL)?> href="<?php echo URL::base(); ?>media" title="Explore media">Explore</a></li>
        <li><a <?=(($active === 'upload') ? ' class="active"' : NULL)?> href="<?php echo URL::base(); ?>media/upload" title="Upload your media">Upload</a></li>
        <li><a <?=(($active === 'profile') ? ' class="active"' : NULL)?> href="<?php echo URL::base(); ?>user/profile" title="View your profile">Profile</a></li>
        <li><a <?=(($active === 'groups') ? ' class="active"' : NULL)?> href="<?php echo URL::base(); ?>user/groups" title="Manage your groups">Groups</a></li>
      </ul>
    </nav><!-- close #mainNav -->
  </div><!-- close .twelvecol -->
  <div class="onecol last"><!-- open column -->
    <nav id="userNav"><!-- open #userNav -->
      <ul>
        <li><a <?=(($active === 'logout') ? ' class="active"' : NULL)?> href="<?php echo URL::base(); ?>login/logout" title="Logout">Logout</a></li>
      </ul>
    </nav><!-- close #userNav -->
  </div><!-- close column -->
</div><!-- close .row -->
