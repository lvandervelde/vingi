<div class="row"><!-- open .row -->
  <div class="sixcol"><!-- open .sixcol -->
    <h2>About</h2>
    <p>
		Veni, Vidi, Vingi
    </p>
  </div><!-- close .sixcol -->

  <div class="sixcol last"><!-- open .sixcol -->
    <h2>Sitemap</h2>
    <ul>
      <li><a href="<?php echo URL::base(); ?>home" title="Home">Home</a></li>
      <li><a href="<?php echo URL::base(); ?>user/profile" title="Profile">Profile</a></li>
    </ul>
  </div><!-- close .sixcol -->
</div><!-- close .row -->