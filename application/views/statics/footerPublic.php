<div class="row"><!-- open .row -->
  <div class="sixcol"><!-- open .sixcol -->
    <h2>About</h2>
    <p>
			Vingi gives you the freedom to share your images and photographs with
			anyone you want. Create groups and albums to easily organize
			your content, while keeping it save from the rest of the world.
    </p>
  </div><!-- close .sixcol -->

  <div class="sixcol last"><!-- open .sixcol -->
    <h2>Account</h2>
    <ul>
      <li><a href="<?php echo URL::base(); ?>login/register" title="Register your company">Register your group</a></li>
      <li><a href="<?php echo URL::base(); ?>" title="Recover your password">Password recovery</a></li>
    </ul>
  </div><!-- close .sixcol -->
</div><!-- close .row -->