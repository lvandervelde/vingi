<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8">
    <meta name="keywords" content="<?php echo $meta_keywords; ?>">
    <meta name="description" content="<?php echo $meta_description; ?>">

    <title><?php echo $title; ?></title>

    <?php
    foreach ($styles as $file => $type) {
      echo HTML::style($file, array('media' => $type));
    }
    ?>
    <?php
    foreach ($scripts as $file) {
      echo HTML::script($file, NULL, TRUE);
    }
    ?>
    
    <script> var root = "<?php echo URL::base(); ?>";</script>
  </head>

  <body class="<?php echo "$controller $action"; ?>">
    <div class="container"><!-- open .container -->

      <div id="main"><!-- open #main -->
        <?php echo $content; ?>
      </div><!-- close #main -->

      <footer id="footer" class="no-print"><!-- open #footer -->
        <?php echo $footer; ?>
      </footer><!-- close #footer -->
    </div><!-- close .container -->
  </body>
</html>
