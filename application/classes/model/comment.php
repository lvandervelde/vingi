<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Comment extends ORM {

  private static $model = 'comment';
  protected $_table_name = 'comment';
  protected $_primary_key = 'id';
  protected $_belongs_to = array('media' => array('foreign_key' => 'mediaId'), 'user' => array('foreign_key' => 'userId'));

  public static function factory($id = null) {
    return empty($id) ? parent::factory(self::$model) : parent::factory(self::$model, $id);
  }

  /**
   *
   * @param type $id
   * @return type 
   */
  public static function getById($id) {
    return self::factory($id);
  }

  /**
   *
   * @param type $mediaId
   * @return type 
   */
  public static function getByMediaId($mediaId) {
    return self::factory()->where('mediaId', '=', $mediaId)->find_all();
  }

  /**
   * 
   */
  public function isPublisher(){
      $session = session::instance();
      return $this->user->id === $session->get('userId');
  }
  
  /**
   * 
   * Generate an associative array based on the comment
   * 
   * @return array
   */
  public function to_array() {
    $assoc_array = array('comment' =>
        array(
            'id' => $this->id,
            'text' => $this->text,
            'mediaId' => $this->mediaId,
            'creationDate' => $this->creationDate,
            'lastEditDate' => $this->lastEditDate,
            'owner' => Model_User::getById($this->userId)->to_array()
        )
    );

    return $assoc_array;
  }

  /**
   *
   * Generate a json string based on the comment
   *
   * @return string
   */
  public function to_json() {
    return json_encode($this->to_array());
  }

}