<?php defined('SYSPATH') or die('No direct script access.');

class Model_Invite extends ORM
{		

	private static $model = 'invite';
	protected $_table_name = 'invite';
	protected $_primary_key = 'id';
	protected $_belongs_to = array('group' => array('foreign_key' => 'groupId'), 'user' => array('foreign_key' => 'email'));
	
	public static function factory($id = null) {
		return empty($id) ? parent::factory(self::$model) : parent::factory(self::$model, $id);
	}
	
	/**
	 *
	 * @param type $id
	 * @return type
	 */
	public static function getById($id) {
		return self::factory($id);
	}	
	
	public static function getByUnique($unique) {
		return self::factory()
					->where('inviteUnique', '=', $unique)
					->find();
	}
	
	/**
	 *
	 * @param type $userId
	 * @return type
	 */
	public static function getByGroupIdAndEmail($email, $groupId) {
		return self::factory()
				->where('groupId', '=', $groupId)
				->where('email', '=', $email)
				->find();
	}
	
	public static function exists($email, $groupId){
		$invite = self::factory()
					->where('email', '=', $email)
					->where('groupId', '=', $groupId)
					->find();
		return $invite->email == $email && $invite->groupId == $groupId;
	}
	
	/**
	 *
	 * Generate an associative array based on the comment
	 *
	 * @return array
	 */
	public function to_array() {
		$assoc_array = array('invite' =>
				array(
						'id' => $this->id,
						'text' => $this->text,
						'groupId' => $this->groupId,
						'email' => $this->email,
				)
		);
	
		return $assoc_array;
	}
	
	/**
	 *
	 * Generate a json string based on the comment
	 *
	 * @return string
	 */
	public function to_json() {
		return json_encode($this->to_array());
	}
}