<?php defined('SYSPATH') or die('No direct script access.');

class Model_Usermedia extends ORM
{

    protected $_belongs_to = array('user' => array('foreign_key' => 'userId'), 'media' => array('foreign_key' => 'mediaId'));
	
}