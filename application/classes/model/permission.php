<?php defined('SYSPATH') or die('No direct script access.');

class Model_Permission extends ORM
{
	// Permission Constants
	const read = 1;
	
	protected $_primary_key = 'id';
	protected $_table_name = 'permission';
	
	public static function factory($id = null) {
		return empty($id) ? parent::factory(self::$model) : parent::factory(self::$model, $id);
	}
	
	public static function getById($id) {
		return self::factory($id);
	}
	
}