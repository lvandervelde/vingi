<?php defined('SYSPATH') or die('No direct script access.');

class Model_Tag extends ORM
{
	
	private static $model = 'tag';
	
	protected $_table_name = 'tag';
	protected $_primary_key = 'id';
	
	protected $_has_many = array(
									'medias' => array(
														'model' => 'media',
														'far_key' => 'mediaId',
														'foreign_key' => 'tagId',
														'through' => 'mediatag'
											)
							);

#TODO Validation rules have to be added. 
	
	public static function factory($id = null) {
		return empty($id) ? parent::factory(self::$model) : parent::factory(self::$model, $id);
	}
	
	public static function getById($id) {
		return empty($id) ? false : self::factory($id);
	}
	
	public static function getTagsLike($string) {
		return empty($string) ? false : self::factory()->where('name', 'like', $string)->find();
	}
/*	
	public static function getPopularTags($amount) {
		DB::select(array('count(*)', 'amount'), array('tag.name', 'name'))->from('tag')->join('mediatag')->on('tag.id', '=', 'mediatag.tagId');
	}*/
}