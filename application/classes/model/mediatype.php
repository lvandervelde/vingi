<?php defined('SYSPATH') or die('No direct script access.');

class Model_Mediatype extends ORM
{
	
	protected $_primary_key = 'id';
	protected $_table_name = 'mediatype';

	protected $_has_many = array('media' => array('foreign_key' => 'id'));
	
}