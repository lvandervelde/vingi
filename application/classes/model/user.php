<?php

defined('SYSPATH') or die('No direct script access.');

class Model_User extends ORM {

    private static $model = 'user';
    protected $_table_name = 'user';
    protected $_primary_key = 'id';
    protected $_has_many = array(
        'ownedGroups' => array(
            'model' => 'group',
            'foreign_key' => 'ownerId'
        ),
        'groups' => array(
            'model' => 'group',
            'far_key' => 'groupId',
            'foreign_key' => 'userId',
            'through' => 'usergroup'
        ),
        'medias' => array(
            'model' => 'media',
            'foreign_key' => 'userId'
        ),
    	'invites' => array(
    		'model' => 'invite',
    		'foreign_key' => 'email'
    	)
    );

#TODO Validation rules have to be added.

    /**
     *
     * Factory function that creates an empty user or loads an user into the object depending on the parameter.
     *
     * @param string $id
     * @return ORM
     */
    public static function factory($id = null) {
        return empty($id) ? parent::factory(self::$model) : parent::factory(self::$model, $id);
    }

    /**
     *
     * Gets a user based on id. Is just a little wrapper around the factory function.
     *
     * @param unknown $id
     * @return ORM
     */
    public static function getById($id) {
        return parent::factory(self::$model, $id);
    }
	
    /**
     * 
     * @param Model_User $user
     * @return string 
     */
    public function getGender() {
    	if($this->gender == 0) {
    		return "Male";
    	}
    	else {
    		return "Female";
    	}
    }

    /**
     * 
     * @param Model_User $user
     * @return string 
     */
    public function getAvatar() {
    	if($this->gender == 0) {
    		return "assets/img/avatar.jpg";
    	}
    	else {
    		return "assets/img/avatar_female.jpg";
    	}
    }

    /**
     *
     * Gets a user based on email.
     *
     * @param unknown $id
     * @return ORM
     */
    public static function getByEmail($email) {
        return self::factory()->where("email", "=", $email)->find();
    }

    /**
     *
     * Gets a user based on unique. 
     * 
     * @param unknown $id
     * @return ORM
     */
    public static function getByUnique($unique) {
        return self::factory()->where("userUnique", "=", $unique)->find();
    }

    /**
     * 
     * This function is used to login a user based on the email and a password given. First gets the users by username and then checks if the passwords are the same.
     * The password hash is generated based on Bcrypt. If both username and password are verified a sessions is made and the User object is returned.
     * Else the function will return false.
     * 
     * @param string $email
     * @param string $password
     * @return User or false
     */
    public static function login($email, $password, $mobile = false) {

        if (!empty($email) && !empty($password)) {
            $user = self::factory()->where("email", "=", $email)->find();

            if ($user->loaded()) {
                $bcrypt = new Helper_Bcrypt(Kohana::$config->load('site')->get('costFactor'));

                if ($bcrypt->verify($password, $user->password)) {

                	if($mobile){
	                	$user->sessionId = Model_User::generateSessionId();
	                	$user->save();
                	}
                	
                    $session = Session::instance();
                    $session->set('userId', $user->id);

                    return $user;
                }
            }
        }
        return false;
    }

    /**
     * Get all media from mutual groups.
     * FOR LIVE STREAM PURPOSES!!
     * @param Model_User $user
     */
    public function getMediasOfMutualGroups(Model_User $user) {
    	return ORM::factory('media')
                    ->join('groupmedia')->on('media.id', '=', 'groupmedia.mediaId')
                    ->join('usergroup` as `UG1')->on('groupmedia.groupId', '=', 'UG1.groupId')->on('UG1.userId', '=', DB::expr($this->id))
                    ->join('usergroup` as `UG2')->on('groupmedia.groupId', '=', 'UG2.groupId')->on('UG2.userId', '=', DB::expr($user->id))
                    ->order_by('media.creationDate', 'desc')
                    ->find_all();
    }
    
    /**
     * Get all media from mutual single user within mutual groups.
     * FOR LIVE PROFILE PURPOSES!!
     * @param Model_User $user
     */
    public function getMediasOfMutualGroupsByUser(Model_User $user, $limit) {
    	return ORM::factory('media')
                    ->join('groupmedia')->on('media.id', '=', 'groupmedia.mediaId')
                    ->join('usergroup` as `UG1')->on('groupmedia.groupId', '=', 'UG1.groupId')->on('UG1.userId', '=', DB::expr($this->id))
                    ->join('usergroup` as `UG2')->on('groupmedia.groupId', '=', 'UG2.groupId')->on('UG2.userId', '=', DB::expr($user->id))
                    ->where('media.userId', '=', $user->id)
                    ->order_by('media.creationDate', 'desc')
                    ->distinct('media.id')
                    ->limit($limit)
                    ->find_all();
    }
    
    /**
     * Get all groups from mutual group partners.
     * @param Model_User $user
     */
    public function getGroupsOfMutualGroupPartners(Model_User $user) {
    	return ORM::factory('group')
                    ->join('usergroup` AS `UG1')->on('group.id', '=', 'UG1.groupId')->on('UG1.userId', '=', DB::expr($this->id))
                    ->join('usergroup` AS `UG2')->on('group.id', '=', 'UG2.groupId')->on('UG2.userId', '=', DB::expr($user->id))
                    ->find_all();
    }
    
    
    public function getInvites() {
    	// $this->invites->find_all();
    	return ORM::factory('invite')
    				->where('email', '=', $this->email)
    				->find_all();
    }

    /**
     * 
     * This function will logout the user
     * 
     * @param Model_User $user
     */
    public function logout(Model_User $user) {

        if (!empty($user)) {
            $session = Session::instance();
            $session->delete('userId');
        }
    }
    
    public function getAlbums(){
        ORM::factory('album')
                ->join('group')->on('album.groupId', '=', 'group.id')
                ->join('usergroup')->on('group.id', '=', 'usergroup.groupId')
                ->where('usergroup.userId', '=', $this->id)
                ->find_all();
    }

    /**
     * 
     * Get all groups from the user
     * 
     */
    public function getGroups() {
        return $this->groups->find_all();
    }
    

    /**
     * 
     * Get all groups where this user is the owner
     * 
     */
    public function getOwnedGroups() {
        return $this->ownedGroups->find_all();
    }

    /**
     * 
     * Get all medias from the user
     * 
     */
    public function getMedias($limit = 24) {
        return $this->medias->order_by("id", "desc")->limit($limit)->find_all();
    }
    
    /**
     * 
     * @return type 
     */
    public static function getLatestFromGroups(){
        return ORM::factory('media')
                ->join('groupmedia')->on('media.id', '=', 'groupmedia.mediaId')
                ->join('usergroup')->on('groupmedia.groupId', '=', 'usergroup.groupId')->on('usergroup.userId', '=', $this->id)
                ->find_all();
    }

    /**
     * 
     * Generate a unique string for this user. This string is used for the directory where his images will be strored.
     * 
     * @return string
     */
    public static function generateUnique() {

        $random = substr(md5(microtime(true) . mt_rand(10000, 90000)), 0, 12);

		if (self::getByUnique($random)->loaded()) {
          	return $this->generateUnique();
		} else {
	   		return $random;
     	}
    }
    
    /**
     * 
     * Function that generates a session id.
     * 
     * @return sessionId
     */
    public static function generateSessionId() {
        $sessionId = '';
        $values = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 'a', 'b', 'c', 'd', 'e', 'f');

        while (strlen($sessionId) < 15) {
            $sessionId .= $values[rand(0, count($values) - 1)];
        }

        return $sessionId;
    }
    
    public function to_array() {
    	$assoc_array = array('user' =>
    			array(
    					'firstname' => $this->firstname,
    					'lastname' => $this->lastname,
    					'password' => $this->password,
    					'email' => $this->email,
    					'userUnique' => $this->userUnique,
    					'phonenumber' => $this->phonenumber,
    					'registerDate' => $this->registerDate,
    					'sessionId' => $this->sessionId
    			)
    	);
    	return $assoc_array;
    }

    /**
     * 
     * Generate a json string based on the user
     * 
     * @return string
     */
    public function to_json() {
        return json_encode($this->to_array());
    }
    
    /**
     * Get popular albums from groups of the current user
     * @return type 
     */
    public function getPopularAlbums(){
        return ORM::factory('album')
                    ->join('albummedia')->on('album.id', '=', 'albummedia.albumId')
                    ->join('group')->on('group.id', '=', 'album.groupId')
                    ->join('usergroup')->on('usergroup.groupId', '=', 'group.id')
                    ->select(array(DB::expr('COUNT(albummedia.mediaId)'), 'media'))
                    ->where('usergroup.userId', '=', $this->id)
                    ->group_by('albummedia.albumId')
                    ->limit(6)
                    ->distinct('album.id')
                    ->order_by('media', 'desc')
                    ->find_all();
    }
    
    /**
     * Get media this user can see, according to the given filters
     * 
     * @param Model_Media $media (to get older/newer media)
     * @param int $limit
     * @param string $search
     * @param int $group
     * @param string $orderby
     * @param string $operator ( < or > )
     * @return dbresult $medias 
     */
    public function getUserMedia($media = null, $limit = null, $search = null, $group = null, $orderby = null, $operator = null){
        $medias = ORM::factory('media')
                ->join('groupmedia')->on('media.id', '=', 'groupmedia.mediaId')
                ->join('group')->on('groupmedia.groupId' ,'=', 'group.id')
                ->join('usergroup')->on('usergroup.groupId' ,'=', 'group.id')
                ->where('usergroup.userId', '=', $this->id)
                ->distinct('media.id');
        
        if($orderby){
            $medias->order_by($orderby, 'desc');
        }
        if($media){
            if($orderby == 'media.views'){
                $medias->where('media.views', $operator, $media->views);
            }else{
                $medias->where('media.id', $operator, $media->id);
            }
        }
        if($limit){
            $medias->limit($limit);
        }
        if($group){
            $medias->where('group.id', '=', $group);
        }
        if($search){
            $medias->where_open()
                   ->where('media.name', 'LIKE', '%'.$search.'%')
                   ->or_where('media.description', 'LIKE', '%'.$search.'%')
                   ->where_close();
            //TODO tags LIKE search
        }
                
        return $medias->find_all();
    }
    
    /**
     * Get the next and prevous record from a list of Media objects
     * @param int $mediaId 
     * @return Model_Media prev and next $media
     */
    public function getNextAndPrevious($mediaId){
        $allMedia = $this->getUserMedia();
        $totalMedia = count($allMedia);
        $thisKey = null;
        
        /**
         * Find the key of the object with 
         * the given mediaId.
         */
        foreach($allMedia as $key => $media)
        {
            //echo $key."/".$totalMedia.": ".$media->id." == ".$mediaId."<br/>";
            
            if($media->id == $mediaId)
            {
                $thisKey = ($key);
            }
        }
        /**
         * If the object is found in the list, find 
         * the next and previous media in the list.
         */
        if(!is_null($thisKey)){
            if($totalMedia > 2){
                //if !next goto first
                //if !previous goto last
                $previous = ($thisKey-1) < 0 ? ($totalMedia-1) : ($thisKey-1);
                $next = ($thisKey+1) >= ($totalMedia) ? 0 : ($thisKey+1);

                return array($allMedia[$previous], $allMedia[$thisKey], $allMedia[$next] );
            }else{
                return false;
            }
            
        }else{
            return false;
        }
        
        
    }

}
