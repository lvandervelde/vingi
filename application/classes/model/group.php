<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Group extends ORM {

    private static $model = 'group';
    protected $_table_name = 'group';
    protected $_primary_key = 'id';
    protected $_belongs_to = array('user' => array('foreign' => 'ownerId'));
    protected $_has_one = array('media' => array('foreign' => 'groupImage'));
    protected $_has_many = array(
        'albums' => array(
            'foreign_key' => 'groupId',
        ),
        'users' => array(
            'model' => 'user',
            'far_key' => 'userId',
            'foreign_key' => 'groupId',
            'through' => 'usergroup'
        ),
        'medias' => array(
            'model' => 'media',
            'far_key' => 'mediaId',
            'foreign_key' => 'groupId',
            'through' => 'groupmedia'
        ),
    	'invites' => array(
    		'model' => 'invite',
    		'foreign_key' => 'groupId',
    	)
    );

    #TODO Validation rules have to be added.
    #TODO Some functions could be converted to 1 (remove and add)

    /**
     * 
     * Factory function that creates an empty group or loads an group into the object depending on the parameter. 
     * 
     * @param string $id
     * @return ORM
     */
    public static function factory($id = null) {
        return empty($id) ? parent::factory(self::$model) : parent::factory(self::$model, $id);
    }
	
    
    /**
     * Returns a image given to a corrospekoefeofd group
     * @param unknown $mediaId
     */
    public function getGroupImage($imageType = null) {
            $media = Model_Media::getById($this->groupImage);
            return $media->getMediaPath($imageType, $media->user);
    }
    
    
    public function getGroupNameEncode() {
    	$name = urlencode($this->name);
    	$encName = str_replace('+', '-', $name);
    	return $encName;
    }
    
    /**
	*
	* @param unknown $mediaId
     */
    public function getGroupOwner() {
    	return Model_User::getById($this->ownerId);
    }
	
    /**
     * 
     * Gets a group based on id. Is just a little wrapper around the factory function.
     * 
     * @param unknown $id
     * @return ORM
     */
    public static function getById($id) {
        return self::factory($id);
    }
    
    /**
     * Returns if the given userId is the same as the owner
     * @param unknown $userId
     * @return boolean
     */
    public function isOwner($userId){
    	return $this->ownerId == $userId;
    }

    /**
     * 
     * Adds an user to the group.
     * 
     * @param Model_User $user
     * @return boolean
     */
    public function addUser(Model_User $user) {

        try {
            if (!empty($user) && $user->loaded()) {
                $this->add('users', $user);
                return true;
            }
            return false;
        } catch (Exception $e) {
            #TODO Catch the right excpetion and handle them
        }
    }

    /**
     * 
     * Removes a user from the group.
     * 
     * @param Model_User $user
     * @return boolean
     */
    public function removeUser(Model_User $user) {

        try {
            if (!empty($user) && $user->loaded()) {
                $this->remove('users', $user);
                return true;
            }
            return false;
        } catch (Exception $e) {
            #TODO Catch the right excpetion and handle them
        }
    }

    /**
     * 
     * Gets all users from the group
     * 
     */
    public function getUsers() {
        return $this->users->find_all();
    }
    
    public function getMemberCount() {
    	return count($this->getUsers());
    }
    
    public function getUsersWithoutOwner() {
    	return $this->users->where('userId', '!=', $this->ownerId)->find_all();
    }
    
    public function hasUser($userId) {
    	$user = ORM::factory('group')
	    		->join('usergroup')->on('group.id', '=', 'usergroup.groupId')
	    		->where('group.id', '=', $this->id)
	    		->where('usergroup.userId', '=', $userId)
	    		->find();
    	return $user->loaded();
    }

    /**
     * 
     * Adds an album to the group
     * 
     * @param Model_Album $album
     * @return boolean
     */
    public function addAlbum(Model_Album $album) {

        try {
            if (!empty($album) && $album->loaded()) {
                $this->add('albums', $album);
                return true;
            }
            return false;
        } catch (Exception $e) {
            #TODO Catch the right excpetion and handle them
        }
    }

    /**
     * 
     * Removes an album from the group
     * 
     * @param Model_Album $album
     * @return boolean
     */
    public function removeAlbum(Model_Album $album) {

        try {
            if (!empty($album) && $album->loaded()) {
                $this->remove('album', $album);
                return true;
            }
            return false;
        } catch (Exception $e) {
            #TODO Catch the right excpetion and handle them
        }
    }

    /**
     * 
     * Gets all albums from the group
     * 
     */
    public function getAlbums($limit=null) {
        
        if($limit){
            return $this->albums->limit($limit)->find_all();
        }else{
            return $this->albums->find_all();
        }
    }
    
    public function containsMedia(Model_Media $media) {
    	return $this->has('medias', $media);
    }

    /**
     * 
     * Adds a media to the group
     * 
     * @param Model_Media $media
     * @return boolean
     */
    public function addMedia(Model_Media $media) {

        try {
            if (!empty($media) && $media->loaded()) {
                $this->add('medias', $media);
                return true;
            }
            return false;
        } catch (Exception $e) {
            #TODO Catch the right excpetion and handle them
        }
    }

    /**
     * 
     * Removes a media from the group
     * 
     * @param Model_Media $media
     * @return boolean
     */
    public function removeMedia(Model_Media $media) {

        try {
            if (!empty($media) && $media->loaded()) {
                $this->remove('medias', $media);
                return true;
            }
            return false;
        } catch (Exception $e) {
            #TODO Catch the right excpetion and handle them
        }
    }

    /**
     * 
     * Gets all medias from the group
     * 
     */
    public function getMedias($limit, $offset) {
        return $this->medias->limit($limit)->offset($offset)->order_by('media.creationDate', 'desc')->find_all();
    }

    public function to_array() {
        $assoc_array = array(static::$model =>
            array(
                'id' => $this->id,
                'name' => $this->name,
                'description' => $this->description,
                'ownerId' => $this->ownerId,
                'groupImage' => Model_Media::getById($this->groupImage)->to_array()
            )
        );

        return $assoc_array;
    }

    public function to_json() {
    	return json_encode($this->to_array());
    }
    
    public static function getPopularGroups(){
        return ORM::factory('group')
                    ->join('usergroup')->on('group.id', '=', 'usergroup.groupId')
                    ->select(array(DB::expr('COUNT(usergroup.userId)'), 'members'))
                    ->group_by('usergroup.groupId')
                    ->limit(3)
                    ->order_by('members', 'desc')
                    ->find_all();
    }
    
    /**
     * Save an image as cover of the album
     * @param Model_Album $album
     * @param file $file
     * @return int $mediaId
     */
    public function setGroupImage($file){
    	if(!empty($file) && $file['error'] == 0){
    		$media = Model_Media::factory();
    		$media->userId = $this->ownerId;
    		$media->name = $this->name.' image';
    		$media->description = 'Image of group '.$this->name;
    		$media->save($file);
    
                $this->groupImage = $media->id;
                $this->save();
                
    		return $media;
    	}
    	else{
    		return false;
    	}
    }

}
