<?php defined('SYSPATH') or die('No direct script access.');

class Model_Album extends ORM
{
    private static $model = 'album';
    protected $_table_name = 'album';
    protected $_primary_key = 'id';
    protected $_belongs_to = array('group' => array('foreign_key' => 'groupId'));
    
    protected $_has_one = array('media' => array('foreign' => 'coverImage'));
    protected $_has_many = array(
        'medias' => array(
            'model' => 'media',
            'far_key' => 'mediaId',
            'foreign_key' => 'albumId',
            'through' => 'albummedia'
        )
    );
    
    /**
     * 
     * @param type $id
     * @return type 
     */
    public static function factory($id = null) {
            return empty($id) ? parent::factory(self::$model) : parent::factory(self::$model, $id);
    }

    /**
     * 
     * @param type $id
     * @return type 
     */
    public static function getById($id) {
            return self::factory($id);
    }
    
    /**
     * 
     * @return type 
     */
    public function getMedias(){
        return $this->medias->find_all();
    }
    
    public function getMediaCount() {
    	return count($this->getMedias());
    }
    
    public function addMedia(Model_Media $media){
     	try {
            if (!empty($media) && $media->loaded()) {
                $this->add('medias', $media);
                return true;
            }
            return false;
        } catch (Exception $e) {
            #TODO Catch the right excpetion and handle them
        }
    }
    
    public function removeMedia(Model_Media $media) {
    
    	try {
    		if (!empty($media) && $media->loaded()) {
    			$this->remove('medias', $media);
    			return true;
    		}
    		return false;
    	} catch (Exception $e) {
    		#TODO Catch the right excpetion and handle them
    	}
    }
    
    /**
     * Returns a image given to a corrospekoefeofd group
     * @param unknown $mediaId
     */
    public function getAlbumImage($imageType = null) {
            $media = Model_Media::getById($this->coverImage);
            return $media->getMediaPath($imageType, $media->user);
    }
    
    public function getGroupImagesNotInAlbum() {
    	$subQry = DB::select('albummedia.mediaId')
    					->from('album')
    					->join('albummedia')
    					->on('album.id', '=', 'albummedia.albumId')
    					->on('album.id', '=', DB::expr($this->id));
    	
    	return ORM::factory('media')
    					->join('groupmedia')
    					->on('media.id', '=', 'groupmedia.mediaId')
    					->on('groupmedia.groupId', '=', DB::expr($this->groupId))
    					->where('groupmedia.mediaId', 'NOT IN', $subQry)
    					->find_all();						
    }
    
    public function inGroup(Model_User $user){
        return Model_Group::getById($this->groupId)->has('users', $user);
    
    }
}