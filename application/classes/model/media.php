<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Media extends ORM {

    private static $model = 'media';
    protected $_table_name = 'media';
    protected $_primary_key = 'id';
    protected $_belongs_to = array('mediatype' => array('foreign_key' => 'typeId'), 'user' => array('foreign_key' => 'userId'));
    protected $_has_many = array(
        'comments' => array(
            'model' => 'comment',
            'foreign_key' => 'mediaId'
        ),
        'tags' => array(
            'model' => 'tag',
            'far_key' => 'tagId',
            'foreign_key' => 'mediaId',
            'through' => 'mediatag'
        ),
        'groups' => array(
            'model' => 'group',
            'far_key' => 'groupId',
            'foreign_key' => 'mediaId',
            'through' => 'groupmedia'
        )
    );

    #TODO Validation rules have to be added.

    /**
     *
     * Factory function that creates an empty media or loads a media into the object depending on the parameter.
     *
     * @param string $id
     * @return ORM
     */
    public static function factory($id = null) {
        return empty($id) ? parent::factory(self::$model) : parent::factory(self::$model, $id);
    }

    /**
     *
     * Gets a media based on id. Is just a little wrapper around the factory function.
     *
     * @param unknown $id
     * @return ORM
     */
    public static function getById($id) {
        return self::factory($id);
    }
    
    /**
     *
     * @return type 
     */
    public static function getAll() {
        return self::factory()->find_all();
    }
    
    /**
     *
     * @param Model_Media $media
     * @return type 
     */
    public function getAlbum(){
        return ORM::factory('album')
                    ->join('albummedia')->on('album.id', '=', 'albummedia.albumId')
                    ->where('albummedia.mediaId', '=', $this->id)
                    ->find();
    }

    /**
     * 
     * Get's a media based on the medias unique and a user. 
     * The mediaUnique value in the database isn't unique by itself. It is only unique in combination with a user.
     * 
     * @param string $unique
     * @param Model_User $user
     * @return Ambigous <ORM, Database_Result, ORM, object, mixed, number, Database_Result_Cached, multitype:>
     */
    public static function getByUnique($unique, Model_User $user) {
        return self::factory()->where('mediaUnique', '=', $unique)->where('userId', '=', $user->id)->find();
    }

    /**
     * 
     * Saves or updates the media. Also saves the uploaded media if it is a new media.
     * 
     * @return ORM|false
     */
    public function save($file = null) {  
    	if (!empty($file) && empty($this->id)) {

    		$user = Model_User::getById($this->userId);
    		
            $this->mediaUnique = $this->generateUnique($user);
            $userDir = Helper_Image::createDirectory($user->userUnique);
            $mediaDir = Helper_Image::createDirectory($this->mediaUnique);

            $fileDir = $userDir . $mediaDir;
            $filename = $this->mediaUnique;
           
            if($originalImage = $this->saveUploadedImage($file, $filename, $fileDir)) {
            	
            	$this->size = $file['size'];
            	$this->width = $originalImage->width;
            	$this->height = $originalImage->height;
            	$this->typeId = 1;
            	
            	return parent::save();
            }
        }
        return parent::save();
    }

    /**
     * 
     * Adds a tag for this media
     * 
     * @param Model_Tag $tag
     * @return boolean
     */
    public function addTag(Model_Tag $tag) {
        try {
            if (!empty($tag) && $tag->loaded()) {
                $this->add('tags', $tag);
                return true;
            }
            return false;
        } catch (Exception $e) {
            #TODO Catch the right excpetion and handle them
        }
    }

    /**
     * 
     * Removes a tag for this media
     * 
     * @param Model_Tag $tag
     * @return boolean
     */
    public function removeTag(Model_Tag $tag) {

        try {
            if (!empty($tag) && $tag->loaded()) {
                $this->remove('tags', $tag);
                return true;
            }
            return false;
        } catch (Exception $e) {
                #TODO Catch the right excpetion and handle them
        }
    }
    
    /**
     * Remove all tags for this media 
     */
    public function removeAllTags(){
        $this->remove('tags');
    }

    /**
     * 
     * Get's all tags for this media
     * 
     */
    public function getTags() {
        return $this->tags
                    ->select(array('name', 'text'), array(DB::expr('COUNT(name)'), 'weight'))
                    ->group_by('name')
                    ->find_all();
    }
    
    /**
     * Get all tags of the current media as string 
     */
    public function getTagString(){
        $tagString = '';
        
        foreach($this->getTags() as $tag){
            if($tag->name){
                $tagString .= $tag->name.', ';
            }
        }
        
        return $tagString;
    }
    
    /**
     * 
     * @return type 
     */
    public static function getAllTags($like = null){
        $tags = ORM::factory('tag')
                    ->select(array('name', 'text'), array(DB::expr('COUNT(name)'), 'weight'))
                    ->join('mediatag')->on('tag.id', '=', 'mediatag.tagId')
                    ->group_by('name')
                    ->limit(25);
        
        if($like){
            $tags->where('tag.name', 'LIKE', '%'.$like.'%');
        }
        
        return $tags->find_all();
    }

    /**
     * 
     * Adds a comment for this media
     * 
     * @param Model_Comment $comment
     * @return boolean
     */
    public function addComment(Model_Comment $comment) {
        try {
            if (!empty($comment) && $comment->loadded()) {
                $this->add('comments', $comment);
                return true;
            }
            return false;
        } catch (Exception $e) {
            #TODO Catch the right excpetion and handle them
        }
    }

    /**
     * 
     * Removes a comment for this media 
     * 
     * @param Model_Comment $comment
     * @return boolean
     */
    public function removeComment(Model_Comment $comment) {

        try {
            if (!empty($comment) && $comment->loaded()) {
                $comment->delete();
                return true;
            }
            return false;
        } catch (Exception $e) {
            #TODO Catch the right excpetion and handle them
        }
    }

    /**
     * 
     * Get's all comments for this media
     * 
     */
    public function getComments() {
        return $this->comments->find_all();
    }
    
    /**
     * 
     */
    public function getLastComment(){
        return $this->comments->limit(1)->order_by('id', 'desc')->find_all();
    }

    /**
     * 
     * Saves the uploaded images to the disk. Also saves a thumbnail, small and large image.
     * 
     * Returns the media object if all images were saved else it returns false.
     * 
     * @param Array of $_FILES $image
     * @param string $filename
     * @param string $dir
     * @param string $extension
     * @return Image|boolean
     */
    private function saveUploadedImage($image, $filename, $dir, $extension = 'jpeg') {

        $thumbnail = Kohana::$config->load('site')->get('thumbnail');
        $small = Kohana::$config->load('site')->get('small');
        $large = Kohana::$config->load('site')->get('large');

        $imageRoot = Kohana::$config->load('site')->get('imageRoot');
        
        $old = umask(0);
        if(!is_dir($imageRoot)) {
        	mkdir($imageRoot, 0775, false);
        }
        umask($old);
        
        $dir = $imageRoot . $dir;

        if (Upload::valid($image) && Upload::not_empty($image) && Upload::type($image, Kohana::$config->load('site')->get('allowedImages'))) {

            $old = umask(0);
            if(mkdir($dir, 0775, true)) {
            	
            	$original = Image::factory($image['tmp_name']);
            	
            	if ($original->save($dir.$filename.".".$extension)) {  
            		
            		umask($old);
            		
            		Image::factory($image['tmp_name'])
            		->resize($thumbnail, $thumbnail, Image::INVERSE)
            		->crop($thumbnail, $thumbnail)
            		->save($dir.$filename.'.thumb.'.$extension);
            		
            		if($original->height < $small || $original->width < $small) {
            			$original->save($dir.$filename.'.small.'.$extension);
            		} else {
            			Image::factory($image['tmp_name'])
            			->resize($small, $small, Image::INVERSE)
            			->save($dir . $filename . '.small.'.$extension);
            		}
            		 
            		if($original->height < $large || $original->width < $large) {
            			$original->save($dir.$filename.'.large.'.$extension);
            		} else {
            			Image::factory($image['tmp_name'])
            			->resize($large, $large, Image::INVERSE)
            			->save($dir . $filename . '.large.'.$extension);
            		}
            		
            		return $original;
            	}
            }
            
            umask($old);
        }
         
        return false;
    }
    
    /**
     * 
     * This function returns the public media path of the media.
     * It accepts two parameters but they are not required.
     * 
     * The first parameter is used get different types of images like the small or thumbnail.
     * 
     * The second parameter is used to add the user unique to the path. This is needed for the App.
     * 
     * If no parameters are given the original image path will be returned.
     * 
     * @param string $imageType
     * @param Model_User $user
     * @return string
     */
    public function getMediaPath($imageType = null, Model_User $user = null) {
    	
    	$url = 'image/'.$this->mediaUnique;
    	$imageTypes = Kohana::$config->load('site')->get('imageTypes');
    	
    	$url .= Model_User::getById($this->userId)->userUnique;
    	
    	if(!empty($imageType) && in_array($imageType, $imageTypes)) {
    		$url .= '/'. $imageType;
    	}
    	
    	return URL::site($url);
    }
     /**
      * 
      * Get's the internal path of a media.
      * 
      * @param string $imageType
      * @return string
      */
    public function getInternalPath($imageType = null) {
    	
    	$user = Model_User::getById($this->userId);
    	$imageRoot = Kohana::$config->load('site')->get('imageRoot');
    	$imageTypes = Kohana::$config->load('site')->get('imageTypes');
    	
    	$userDir = Helper_Image::createDirectory($user->userUnique);
    	$imageDir = Helper_Image::createDirectory($this->mediaUnique);
    	
    	return empty($imageType) && !in_array($imageType, $imageTypes) ? $imageRoot.$userDir.$imageDir.$this->mediaUnique.'.jpeg' : $imageRoot.$userDir.$imageDir.$this->mediaUnique.'.'.$imageType.'.jpeg';
    }
    
    /**
     *
     * Get's the internal path of a media.
     *
     * @param string $imageType
     * @return string
     */
    public function getPublicPath() {
    	$user = Model_User::getById($this->userId);
    	return url::base()."media/view/".$this->mediaUnique.$user->userUnique;
    }
	
    /**
     * 
     * Generates a unique twelve character long string for this users media.
     * It is used as an indentifier for the media.
     * 
     * @param Model_User $user
     * @return string
     */
    private function generateUnique(Model_User $user) {
        $random = substr(md5(microtime(true) . mt_rand(10000, 90000)), 0, 12);

        if (empty(self::getByUnique($random, $user)->id)) {
            return $random;
        } else {
            return $this->generateUnique($user);
        }
    }
    
    public static function getBetween($startId, $endId){
    	return ORM::factory('media')->where('media.id', 'BETWEEN', array($startId, $endId))->find_all();
    }
    
    /**
     * 
     * Generate an associative array based on the media
     * 
     * @return array
     */
    public function to_array() {
        $assoc_array = array('media' =>
            array(
                'id' => $this->id,
                'mediaUnique' => $this->mediaUnique,
                'name' => $this->name,
                'description' => $this->description,
                'userId' => $this->userId,
                'typeId' => $this->typeId,
                'width' => $this->width,
                'height' => $this->height,
                'size' => $this->size,
                'creationDate' => $this->creationDate,
            	'user' => Model_User::getById($this->userId)->to_array()
            )
        );

        return $assoc_array;
    }
    
    /**
     *
     * Generate a json string based on the media
     *
     * @return string
     */
    public function to_json() {
    	return json_encode($this->to_array());
    }

}
