<?php
/**
 * Description of invite controller
 * Controller for handling all the requests needed
 * for the invite functionality.
 *
 * @author Vingi team
 * 
 * @date 29-10-2012
 */
defined('SYSPATH') or die('No direct script access.');

class Controller_Invite extends Controller_PrivateTemplate {

    private $view;

    public function action_index() {
        $this->request->redirect('home');
    }
    
    /**
     * Display a form for inviting new users 
     */
    public function action_view(){
        $invitedId = $this->request->param('id');
        $invite = Model_Invite::getById($invitedId);
        
        if(!$this->validInvite($invite->email)) {
        	$this->request->redirect('user/profile/');
        }
        
        $this->view = new View('pages/invite');
        
        $this->view->bind('invite', $invite);
        
        $this->template->content = $this->view;
        $this->template->title = "Vingi -". $invite->group->name;
        $this->template->active = 'groups';
    }
    
    /**
     * Action for the invite form 
     */
    public function action_response() {
    	$inviteData = $this->request->post('invite');
        
        $email = $inviteData['email'];
    	$groupId = $inviteData['groupId'];
    	
    	if(!$this->validInvite($email)) {
    		// Invalid invite
    		$this->request->redirect('user/profile/');
    	}    	
    	else if($inviteData['submit'] == 'accept') {
    		$this->action_accept($email, $groupId);
    	}
    	else if($inviteData['submit'] == 'decline') {
    		$this->action_decline($email, $groupId);
    	}
    	else if($inviteData['submit'] == 'cancel') {
			$this->request->redirect('user/profile/');
    	}
    }
    
    /**
     * Action for accepting an invite
     * @param string $email
     * @param int $groupId 
     */
    private function action_accept($email, $groupId){
    	$session = Session::instance();
    	$sessionUser = Model_User::getById($session->get('userId'));
    	
    	/** Accept **/
    	$invite = Model_Invite::getByGroupIdAndEmail($email, $groupId);
    	$group = Model_Group::getById($groupId);

    	// Add to usergroup
    	$group->addUser($sessionUser);

    	// Remove from database
    	$invite->delete();

    	$this->request->redirect('group/profile/'.$groupId);
    	 
    }
    
    /**
     * Action for declining an invite
     * @param string $email
     * @param int $groupId 
     */
    private function action_decline($email, $groupId){
    	 
    	/** Decline **/
    	$invite = Model_Invite::getByGroupIdAndEmail($email, $groupId);
    	
    	//Remove from database
    	$invite->delete();
    	
    	$this->request->redirect('user/profile/');
    	 
    }
    
    /**
     * Check if the invite is valid
     * @param string $email
     * @return boolean valid
     */
    private function validInvite($email) { 
        return $this->user->email == $email;
    }
}