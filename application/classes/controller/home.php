<?php
/**
 * Description of home controller
 * Controller for handling all the requests needed
 * for the livestream functionality.
 *
 * @author Vingi team
 * 
 * @date 29-10-2012
 */
defined('SYSPATH') or die('No direct script access.');

/**
 * Live stream page with latest uploaders, latest uploads and popular uploads. 
 */
class Controller_Home extends Controller_PrivateTemplate {

    private $view;
    
    public function action_index() {
        $this->template->scripts = array('assets/js/livestream.js');
        
        $this->view = new View('pages/liveStream');
        $this->view->bind('userGroups', $this->user->getGroups());
        
        $this->template->content = $this->view;
        $this->template->title = "Vingi - Livestream";
        $this->template->active = 'home';
    }
    
    /**
     * Ajax call to get media for the livestream 
     */
    public function action_getMedia(){
        $this->auto_render = false;
        
        $mediaId = $this->request->post('media');
        $group = $this->request->post('group');
        $limit = $this->request->post('limit');
        $search = $this->request->post('search');
        $orderby = $this->request->post('orderby');
        $operator = $this->request->post('operator');
        
        /**
         * if mediaId is false, load all media.
         * Else load older or newer media than the given mediaId 
         */
        if( $mediaId != 'false' ){
            $media = Model_Media::getById($mediaId);
        }else{
            $media = null;
        }
        
        $medias = $this->user->getUserMedia($media, $limit, $search, $group, $orderby, $operator);
        
        echo View::factory('pages/_photoGallery')->bind('medias', $medias);
        
    }
}