<?php
/**
 * Description of mobileclient controller
 * Controller for handling all the requests needed
 * for the mobileclient functionality.
 *
 * @author Vingi team
 *
 * @date 29-10-2012
 */
defined('SYSPATH') or die('No direct script access.');

define('REQUEST','request');
define('LOGIN_REQUEST','login');
define('PHOTO_REQUEST','photo');
define('PHOTOS_REQUEST','photos');
define('LATEST_REQUEST','latest');
define('NEW_MEDIAS_REQUEST','newMedias');
define('GROUP_REQUEST','group');
define('GROUPS_REQUEST','groups');
define('COMMENTS_REQUEST','comments');
define('UPLOAD_REQUEST','upload');

define('EMAIL','email');
define('PASSWORD','password');
define('GROUP_ID','groupId');
define('PHOTO_ID','photoId');

define('SESSION_ID','sessionId');
define('MEDIA_UNIQUE','mediaUnique');
define('MEDIA_ID','mediaId');
define('USER_UNIQUE','userUnique');

class Controller_MobileClient extends Controller {

	/**
	 * Handeld de binnenkomende aanvraag af
	 * Aan de hand van de meegegeven parameters wordt een respons gestuurd
	 */
	public function action_index() {

		if(isset($_REQUEST[REQUEST])) {
			$request = $_REQUEST[REQUEST];
		} else {
			echo "Error";
			return;
		}

		switch($request){

			case LOGIN_REQUEST:
				$this->loginRequest();
				break;

			case PHOTO_REQUEST:
				$this->photoRequest();
				break;

			case PHOTOS_REQUEST:
				$this->photosRequest();
				break;

			case UPLOAD_REQUEST:
				$this->uploadRequest();
				break;

			case NEW_MEDIAS_REQUEST:
				$this->newMediasRequest();
				break;

			case LATEST_REQUEST:
				$this->latestRequest();
				break;

			case GROUP_REQUEST:
				$this->groupRequest();
				break;

			case GROUPS_REQUEST:
				$this->groupsRequest();
				break;

			case COMMENTS_REQUEST:
				$this->commentsRequest();
				break;
		}
			
	}

	/**
	 * Login Request
	 *
	 * De gebruiker wordt geverifiëerd aan de hand van een email en een wachtwoord
	 * Als de verificatie succesvol is verlopen worden de gegevens van de gebruiker terug gestuurd
	 */
	private function loginRequest(){

		if(!isset($_REQUEST[EMAIL]) || !isset($_REQUEST[PASSWORD]))	{
			echo "Error";
			return;
		}

		$email = $_REQUEST[EMAIL];
		$password = $_REQUEST[PASSWORD];

		$user = Model_User::login($email, $password, true);

		if($user == null)
			echo "Error";
		else
			echo $user->to_json();
	}

	/**
	 * Upload Request
	 *
	 * Uploading a photo from a user. A user's credentials are first verified, after this
	 * the photo is saved in the given group.
	 */
	private function uploadRequest(){
		echo "UPLOADREQUEST!";
		//var_dump($_FILES);
		$user = $this->verifyUser();
		if(!isset($_FILES["file"]) || !isset($_REQUEST['description']))
			die("Error");

		$media = Model_Media::factory();

		$file = $_FILES["file"];

		$description = $_REQUEST['description'];
		$groupId = $_REQUEST['groupId'];
		$name = $_REQUEST['name'];

		echo "userId: ".$user->id;

		$media->userId = $user->id;
		$media->name = $name;
		$media->description = $description;

		$media->save($file);
		$group = Model_Group::getById($groupId);
		$has = false;
		foreach ($user->getGroups() as $userGroup) {
			if ($userGroup->id == $group->id) {
				$has = true;
				break;
			}
		}
		if (!$has)
			die("Not member of group!");
		$group->addMedia($media);
		echo " saved ";
	}

	/**
	 * Photo Request
	 * 
	 * Requesting a single photo from a photo Id
	 */
	private function photoRequest(){
		$this->verifyUser();
		if(!isset($_REQUEST[PHOTO_ID]))
			die("Error");
		$photoId = $_REQUEST[PHOTO_ID];
		var_dump($photoId);
		$media = Model_Media::getById($photoId);
		if($media->id == null)
			die("Error");
		echo $media->to_json();
	}

	/**
	 * Photos Request
	 * 
	 * Requesting all photo's from a group
	 */
	private function photosRequest(){
		$user = $this->verifyUser();
		var_dump($_REQUEST);
		if(!isset($_REQUEST[GROUP_ID]))
			die("Error");
		$groupId = $_REQUEST[GROUP_ID];
		$group = Model_Group::getById($groupId);

		var_dump($group->getMedias());
		if($group->id == null)
			die("Error");

		echo $group->to_json();
	}

	/**
	 * Groups Request
	 *
	 * Requesting all groups from a user
	 */
	private function groupsRequest(){
		$user = $this->verifyUser();
		$groups = $user->getGroups();
		$response = array();
		foreach($groups as $group){
			array_push($response, $group->to_json());
		}
		echo '{"groups":['.implode(',',$response).']}';
	}

	/**
	 * Group Request
	 *
	 * Requesting a single group with its information
	 */
	private function groupRequest(){
		$user = $this->verifyUser();
		if(!isset($_REQUEST[GROUP_ID]))
			die("Error");
		$groupId = $_REQUEST[GROUP_ID];
		$group = Model_Group::getById($groupId);

		if($group->id == null)
			die("Error");

		echo $group->to_json();
	}

	/**
	 * Comments Request
	 * 
	 * Requesting the comments from a photo
	 */
	private function commentsRequest(){
		$user = $this->verifyUser();
		if(!isset($_REQUEST[MEDIA_ID]))
			die("Error");

		$mediaId = $_REQUEST[MEDIA_ID];
		$media = Model_Media::getById($mediaId);

		$comments = $media->getComments();

		$response = array();
		foreach($comments as $comment){
			array_push($response, $comment->to_array());
		}
		echo json_encode(
				array(
						"comments" => $response
				)
		);
	}

	/**
	 * Latets Request
	 * 
	 * Requesting the latest photo's of a user's live stream
	 */
	private function latestRequest(){
		$user = $this->verifyUser();

		if(isset($_REQUEST[MEDIA_UNIQUE]) && isset($_REQUEST[USER_UNIQUE])) {
			$lastUnique = $_REQUEST[MEDIA_UNIQUE];
			$lastMediaUserUnique = $_REQUEST[USER_UNIQUE];

			$lastMediaUser = Model_User::getByUnique($lastMediaUserUnique);

			$lastMedia = Model_Media::getByUnique($lastUnique, $lastMediaUser);

			$medias = $user->getUserMedia($lastMedia, 45, null, null, 'media.id', '>');
		}else{
			$medias = $user->getUserMedia(null, 45, null, null, 'media.id');
		}

		$response = array();
		foreach($medias as $media){
			array_push($response, $media->to_json());
		}
		echo '{"medias":['.implode(',',$response).']}';
	}

	/**
	 * New Media Request
	 * 
	 * Requesting new media to append at the live stream
	 */
	private function newMediasRequest(){
		$user = $this->verifyUser();

		if(empty($_REQUEST[MEDIA_UNIQUE]) || empty($_REQUEST[USER_UNIQUE]))
			die("Error no identifier");

		$lastUnique = $_REQUEST[MEDIA_UNIQUE];
		$lastMediaUserUnique = $_REQUEST[USER_UNIQUE];

		$lastMediaUser = Model_User::getByUnique($lastMediaUserUnique);

		$lastMedia = Model_Media::getByUnique($lastUnique, $lastMediaUser);

		if($lastMedia->id == null)
			die("Error not loaded");


		$medias = $user->getUserMedia($lastMedia, null, null, null, 'media.id', '>');

		$response = array();
		foreach($medias as $media){
			array_push($response, $media->to_json());
		}
		echo '{"medias":['.implode(',',$response).']}';
	}

	/**
	 * Verify User
	 * 
	 * Verify a user from a given sessionId and email.
	 * If the verification is succesfull the selected user is returned, else an error "VerificationError" is printed
	 * 
	 * @return Model_User $user
	 */
	private function verifyUser(){
		if(!isset($_REQUEST[SESSION_ID]) || !isset($_REQUEST[EMAIL]))	{
			die("VerificationError");
		}
		$email = $_REQUEST[EMAIL];
		$sessionId = $_REQUEST[SESSION_ID];

		$user = Model_User::getByEmail($email);

		if($user->sessionId == $sessionId){
			return $user;
		} else {
			die("VerificationError");
		}
	}
}
