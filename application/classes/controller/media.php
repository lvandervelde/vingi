<?php
/**
 * Description of media controller
 * Controller for handling all the requests needed
 * for the media functionality.
 *
 * @author Vingi team
 * 
 * @date 29-10-2012
 */
defined('SYSPATH') or die('No direct script access.');

class Controller_Media extends Controller_PrivateTemplate {

	private $view;

	/**
	 * Display the explore page
	 */
	public function action_index() {
		$this->view = new View('pages/explore');
		$this->template->scripts = array('assets/js/jqcloud-1.0.2.min.js',
                                                 'assets/js/explore.js');

                $session = session::instance();
                $user = Model_User::getById($session->get('userId'));
                
                $groups = Model_Group::getPopularGroups();
                $albums = Model_User::getById($user->id)->getPopularAlbums();
                $medias = Model_User::getById($user->id)->getUserMedia(null, 6, null, null, 'media.views');
                
                $this->view->bind('user', $user);
		$this->view->bind('groups', $groups);
		$this->view->bind('albums', $albums);
		$this->view->bind('medias', $medias);

		$this->template->content = $this->view;
		$this->template->title = "Vingi - Explore";
		$this->template->active = 'explore';
	}
        
        /**
         * Ajax call to get tags 
         */
        public function action_getTags(){
            $this->auto_render = false;
            
            $mediaId = $this->request->post('mediaId');
            
            /**
             * If a mediaId is posted, get the tags of that media.
             * Else get all available tags. 
             */
            if(!$mediaId){
                $allTags = Model_Media::getAllTags();
            }else{
                $allTags = Model_Media::getById($mediaId)->getTags();
            }
            
            $tags = array();
            $count = 0;
            
            foreach($allTags as $tag){
                $tags[$count]['text'] = $tag->text;
                $tags[$count]['weight'] = $tag->weight;
                $count++;
            }
            
            echo json_encode($tags);
        }

	/**
	 * Show a single media file on a page
	 */
	public function action_view() {
		$uniqueString = $this->request->param('id');

		if(!empty($uniqueString) && strlen($uniqueString) == 24) {

			$imageUnique = null;
			$user = null;

			$imageUnique = substr($uniqueString, 0, 12);
			$userUnique = substr($uniqueString, 12);

			$user = Model_User::getByUnique($userUnique);
			$media = Model_Media::getByUnique($imageUnique, $user);
			
                        /**
                         * Check if the media is loaded at this point
                         * Redirect to the explore view if it isn't. 
                         */
			if ($media->loaded()) {
				$this->view = new View('pages/media');
				$this->incrementViews($media->id);
			
				$comments = $media->getComments();
                                
				$userID = $media->userId;
				$user = Model_User::getById($userID);
				
                                $prevNextMedia = $this->user->getNextAndPrevious($media->id);
                               
                                if($prevNextMedia){
                                    $this->view->bind('prev', $prevNextMedia[0]);
                                    $this->view->bind('next', $prevNextMedia[2]);
                                }
                                $this->view->bind('prevNextMedia', $prevNextMedia);
                                
				$this->view->bind('media', $media);
				$this->view->bind('comments', $comments);
				$this->view->bind('user', $user);
                                $this->view->bind('session', $this->user);
			
				$this->template->content = $this->view;
				$this->template->title = "Vingi - $media->name";
				$this->template->active = 'explore';
				$this->template->scripts = array('assets/js/media.js',
                                                                 'assets/js/jquery.form.js',
                                                                 'assets/js/jqcloud-1.0.2.min.js');
			
			} else {
				$this->request->redirect('media');
			}
		}
		else {
			$this->request->redirect('media');
		}
	}
        
        /**
         * Edit media 
         */
        public function action_edit(){
            $mediaUnique = $this->request->param('id');
            $session = session::instance();
            $user = Model_User::getById($session->get('userId'));
            $media = Model_Media::getByUnique($mediaUnique, $user);
            
            $this->view = new View('pages/mediaEdit');
            
            $this->view->bind('user', $user);
            $this->view->bind('media', $media);
            
            $this->template->title = "Vingi - Edit $media->name";
            $this->template->active = 'explore';
            $this->template->content = $this->view;
        }
        
        /**
         * Process the edit form of this media 
         */
        public function action_editProcess(){
            $mediaData = $this->request->post('media');
            $mediaObj = Model_Media::getByUnique( $mediaData['id'], $this->user );
            $user = Model_User::getById($mediaObj->userId);
            
            $redirect = "media/view/".$mediaObj->mediaUnique.$user->userUnique;
            
            if ($mediaData['submit'] == 'submit' || $mediaData['submit'] == 'next'){
                
                $tags = explode(',', str_replace(' ', '', $mediaData['tags']) );
                                
                $mediaObj->name = $mediaData['name'];
                $mediaObj->description = $mediaData['description'];
                $mediaObj->save();
                
                /**
                 * Set tags of this media 
                 */
                $mediaObj->removeAllTags();
                
                foreach($tags as $tag){
                    $tagObj = Model_Tag::getTagsLike( ucfirst($tag) );
                    
                    if($tagObj){
                        if(!$tagObj->loaded()){
                            $tagObj = Model_Tag::factory();
                            $tagObj->name = ucfirst($tag);
                            $tagObj->save();
                        }
                        
                        $mediaObj->addTag($tagObj);
                    }
                }
                
                $this->request->redirect($redirect, 200);
                
            }elseif($mediaData['submit'] == 'delete'){
                
                $mediaObj->delete();
                $this->request->redirect($redirect, 200);
                
            }else{ //cancel
                $this->request->redirect($redirect, 200);
            }
                
            
        }

	/**
	 * Increase the number of views for the current media item
	 */
	private function incrementViews($mediaId) {
		$media = Model_Media::getById($mediaId);

		if ($media->loaded()) {
			$views = $media->views + 1;
			$media->views = $views;
			$media->save();
		}
	}

	/**
	 * Show the file upload page
	 */
	public function action_upload() {

		$groupName = urldecode(str_replace("-", " ", $this->request->param('id')));
		$albumName = urldecode(str_replace("-", " ", $this->request->param('subid')));
		
		$this->view = new View('pages/mediaUpload');

		if(!empty($groupName)){
			$group = $this->user->groups->where("name", "=", $groupName)->find();
			
			if($group->loaded()){

				$groups = array( $group );
				
				$album = $group->albums->where("name", "=", $albumName)->find();
				
				if($album->loaded()){
					$this->view->bind('album', $album);
				}
			}
			else {
				$groups = $this->user->getGroups();
			}
		}
		else {
			$groups = $this->user->getGroups();
		}
		$this->view->bind('groups', $groups);

		$this->template->title = "Vingi - Upload";
		$this->template->active = 'upload';
		$this->template->content = $this->view;
		$this->template->scripts = array('assets/js/upload.js',
				'assets/js/jquery.form.js');
	}

	/**
	 * The uploading of multiple files from the
	 * file upload page.
	 */
	public function action_uploadProgress() {
		$groupId = $this->request->post('groupId');
		$group = Model_Group::getById($groupId);
		
		$albumId = $this->request->post('albumId');
		$album = $group->albums->where("id", "=", $albumId);
		
		$has = false;
		foreach ($this->user->getGroups() as $userGroup) {
			if ($userGroup->id == $group->id) {
				$has = true;
				break;
			}
		}
		if (!$has)
			die("Not member of group!");

		$name = $this->request->post('name');
		$description = ""; // not nessecary at first $this->request->param('description');

		$file = $_FILES['file'];
		$media = Model_Media::factory();
		$media->userId = $this->user->id;
		$media->name = $name;
		$media->description = $description;

		$media->save($file);

		$group->addMedia($media);
		
		if($album->loaded()){
			$album->addMedia($media);
		}
		
		$this->auto_render = false;
	}

	/**
	 * Add a new comment to a media
	 */
	public function action_addComment() {
		$this->auto_render = false;

		$commentData = $this->request->post('comment');

		$comment = Model_Comment::factory();
		$comment->text = $commentData['content'];
		$comment->mediaId = $commentData['mediaId'];
		$comment->creationDate = date("Y-m-d H:i:s");
		$comment->userId = $this->user->id;
		$comment->save();

		$comment = Model_Media::getById( $commentData['mediaId'] )->getLastComment();

		echo View::factory('pages/_comment')->bind('comments', $comment);
	}
	
        /**
         * Edit an existing comment 
         */
	public function action_editComment() {
		$this->auto_render = false;
		$commentText = $this->request->post('text');
		$commentId = $this->request->post('id');
	
		$comment = Model_Comment::getById($commentId);
	
		if ($comment->loaded()) {
			$comment->text = $commentText;
			$comment->save();
	
			echo json_encode(array('text' => $commentText, 'date' => $comment->lastEditDate));
		}
	}
        
        /**
         * Show the fullscreen page for media 
         */
        public function action_fullscreen() {
            $this->view = new View('pages/mediaFullscreen');
            
            /**
             * Bind the mediaId from where the fullscreen request came 
             */
            $startMediaId = $this->request->param('id');
            $this->view->bind('start', $startMediaId);
            
            $this->template->content = $this->view;
            $this->template->title = "Vingi - Fullscreen";
            $this->template->active = 'explore';
            $this->template->styles = array('assets/css/supersized.shutter.css' => 'screen',);
            $this->template->scripts = array('assets/js/jquery.easing.min.js',
                                             'assets/js/supersized.shutter.min.js',
                                             'assets/js/supersized.3.2.7.min.js');
        }
        
        /**
         * Ajax call to get media for the fullscreen option
         */
        public function action_getFullscreenMedia(){
            $this->auto_render = false;
            $startId = $this->request->post('id');
            
            $limit = 10;
            $media = Model_Media::getById($startId);
            $medias = $this->limit($this->user->getUserMedia(), $limit, $startId);
            
            $userMedia = array();
            $count = 0;
            $startIndex = 0;
            
            /**
             * Create an array for echoing json encoded media 
             */
            foreach($medias as $key => $media){ 
                
                if($media->id == $startId){
                    $startIndex = $key;
                }
                
                $userMedia[$count]['image'] = $media->getMediaPath();
                $userMedia[$count]['title'] = $media->name;
                $userMedia[$count]['thumb'] = $media->getMediaPath('thumb');
                
                $count++;
            }
            
            echo json_encode($userMedia).'::'.$startIndex;
        }
        
        /**
         * Ajax call to get url of the given mediaId 
         */
        public function action_getPath(){
            $this->auto_render = false;
            $mediaId = $this->request->post('id');
            
            $path = Model_Media::getById($mediaId)->getPublicPath();
            
            echo $path;
        }
        
        /**
         * Limit the amount of media shown in the fullscreen media page
         * @param array $medias
         * @param int $limit
         * @param int $startId
         * @return array $medias 
         */
        private function limit($medias, $limit, $startId){
            $counter = 0;
            $startIndex = 0;
            $mediaArray = array();
            
            /**
             * If the given $medias amount is less then the limit
             * just return all of the $medias. 
             */
            if(count($medias) < $limit){
                return $medias;
            }
            
            /**
             * Create an array with media from the startId
             * to the limit amount. 
             */
            foreach($medias as $key => $media){
                
                $mediaArray[] = $media;
                
                if($media->id == $startId){
                    $startIndex = $key;
                }
                
                if($startIndex != 0){
                    $counter++;
                }
            }
            
            /**
             * If the amount of media in the mediarray is less 
             * than then the limit add media from the start. 
             */
            if(($limit - $counter) > 0){
                $startSlice = array_slice($mediaArray, 0, $limit - $counter);
                $medias = array_merge( array_slice($mediaArray, $startIndex, $limit),$startSlice );
            }else{
                $medias = array_slice($mediaArray, $startIndex, $limit);
            }
            
            return $medias;
            
        }

}
