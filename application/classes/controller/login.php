<?php
/**
 * Description of login controller
 * Controller for handling all the requests needed
 * for the login functionality.
 *
 * @author Vingi team
 * 
 * @date 29-10-2012
 */
defined('SYSPATH') or die('No direct script access.');

class Controller_Login extends Controller_PublicTemplate {

    public function action_index() {
    	
    	$userId = Session::instance()->get('userId');
    	$user = Model_User::getById($userId);
    	
    	if($user->loaded()) {
                $this->request->redirect('home');
    	} else {
    		$view = new View('pages/login');
    		$this->template->content = $view;
    	}
    }
    
    /**
     * Try to login with the user information from the
     * login form. 
     */
    public function action_verify()
    {
        $userModel = new Model_User();
        
        $loginData = $this->request->post('login');
        
        if(!$userModel->login($loginData['email'], $loginData['password']))
        {
            $this->request->redirect('login');
        }
        else
        {
            $this->request->redirect('home');
        }
    }
    
    /**
     * Display the registration form 
     */
    public function action_register()
    {
        $view = new View('pages/userCreate');
        $this->template->content = $view;
    }
    
    /**
     * Display the password recovery form 
     */
    public function action_recovery()
    {
        $view = new View('pages/recoverPassword');
        $this->template->content = $view;
    }
    
    /**
     * Action for logging out a user 
     */
    public function action_logout()
    {
        $session = Session::instance();
        $userID = $session->get('userId');
        
        $userModel = new Model_User();
        $user = $userModel->getById($userID);
        
        $userModel->logout($user);
        
        $this->request->redirect('login');
    }
    
    /**
     * Display a form for inviting new users to a group 
     */
    public function action_newUser() {
    	$inviteUnique = $this->request->param('id');
    	$invite = Model_Invite::getByUnique($inviteUnique);
    	
    	if($invite->loaded()) {
	    	$this->view = new View('pages/userNew');
	    	$this->view->bind('invite', $invite);
	    	
	    	$this->template->content = $this->view;
	    	$this->template->title = "Vingi - You are invited to join Vingi";
    	}
    	else {
    		$this->request->redirect('login/');
    	}
    }
    
    public function action_registerNewUser() {
    	$userData = $this->request->post('user');
        
    	$unique = $userData['invite'];
    	$invite = Model_Invite::getByUnique($unique);
    	
    	$userModel = ORM::factory('user')->getByEmail($userData['email']);
    	
    	if (!$userModel->loaded()) {
    		$user = Model_User::factory();
	    	$bcrypt = new Helper_Bcrypt(Kohana::$config->load('site')->get('costFactor'));
	    	$password = $bcrypt->genHash($userData['password']);
	    	
	    	$user->firstname = $userData['firstname'];
	    	$user->lastname = $userData['lastname'];
	    	$user->gender = $userData['gender'];
	    	$user->email = $userData['email'];
	    	$user->phonenumber = $userData['phoneNumber'];
	    	$user->password = $password;
	    	$user->userUnique = $userModel->generateUnique();
	    	$user->sessionId = "0";
	    	$user->save();
	    	
	    	$groupId = $invite->groupId;
	    	$group = Model_Group::getById($groupId);
	    	$group->addUser($user);
	    	
	    	$invite->delete();
	    	
	    	$this->request->redirect('login', 200);
    	}
    	else {
    		$this->request->redirect('login/register', 200);
    	}
    }
    
    
    /**
     * Action for registering a new user and group 
     */
    public function action_registerUser() {
        $this->auto_render = false;
        $userData = $this->request->post('user');
        
        /**
         * Find out if the emailadres is already in use by some other user.
         * If this is the case return a validation error. 
         */
        $userModel = ORM::factory('user')->getByEmail($userData['email']);

        if (!$userModel->loaded()) {
            $user = Model_User::factory();

            /**
             * Encrypt the given password with bcrypt 
             * and register the user.
             */
            $bcrypt = new Helper_Bcrypt(Kohana::$config->load('site')->get('costFactor'));
            $password = $bcrypt->genHash($userData['password']);

            $user->firstname = $userData['firstname'];
            $user->lastname = $userData['lastname'];
            $user->gender = $userData['gender'];
            $user->email = $userData['email'];
            $user->phonenumber = $userData['phoneNumber'];
            $user->password = $password;
            $user->userUnique = $userModel->generateUnique();
            $user->sessionId = "0";
            $user->save();
            
            /**
             * Register the group 
             */
            $groupData = $this->request->post('group');

            $group = Model_Group::factory();
            $group->name = $groupData['name'];
            $group->description = $groupData['description'];
            $group->ownerId = $user->id;
            $group->groupImage = 0;
            $group->save();
            
            if($group->loaded()){
                $group->setGroupImage($_FILES['groupImage']);
                $group->addUser($user);
            }

            $this->request->redirect('login', 200);
        } else {
            $this->request->redirect('login/register', 200);
        }
    }
    
    /**
     * Add an imageCover to the group
     * @param Model_Group $group
     * @param file $file
     * @return int $mediaId 
     */
    public static function setGroupImage(Model_Group $group, $file){
                    
        if(!empty($file) && $file['error'] == 0){
            $media = Model_Media::factory();
            $media->userId = $group->ownerId;
            $media->name = $group->name.' image';
            $media->description = 'Image of group '.$group->name;
            $media->save($file);
           
            return $mediaId;
        }else{
            return false;
        }
    }
}
