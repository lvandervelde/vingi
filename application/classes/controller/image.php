<?php
/**
 * Description of image controller
 * Controller for handling all the requests needed
 * for the image functionality.
 *
 * @author Vingi team
 * 
 * @date 29-10-2012
 */
defined('SYSPATH') or die('No direct script access.');

class Controller_Image extends Controller {

	/**
	 * @throws Kohana_HTTP_Exception_404
	 */
	public function action_index() {

		$uniqueString = $this->request->param('id');
		$imageType = $this->request->param('size');

		if(!empty($uniqueString)) {

			$imageUnique = null;
			$userUnique = null;
			$user = null;

			if(strlen($uniqueString) == 12) {
				$imageUnique = $uniqueString;

				$user = Model_User::getById(Session::instance()->get('userId'));

			} else if(strlen($uniqueString) == 24) {
				$imageUnique = substr($uniqueString, 0, 12);
				$userUnique = substr($uniqueString, 12);

				$user = Model_User::getByUnique($userUnique);
			}

			if(!Model_User::getById(Session::instance()->get('userId'))->loaded()){
				if(!isset($_REQUEST['sessionId']) || !isset($_REQUEST['email'])){
					header('HTTP/1.0 403 Unautherized');
					die();
				}
				$email = $_REQUEST['email'];
				$sessionId = $_REQUEST['sessionId'];

				$user = Model_User::getByEmail($email);
				
				if($user->sessionId != $sessionId){
					if(!isset($_REQUEST['sessionId']) || !isset($_REQUEST['email'])){
						header('HTTP/1.0 403 Unautherized');
						die();
					}
				}


				if($userUnique != null)
					$user = Model_User::getByUnique($userUnique);
			}
				
			$image = Model_Media::getByUnique($imageUnique, $user);
			
			if ($image->loaded()) {
				$imagePath = $image->getInternalPath($imageType);
				$this->response->headers('content-type', File::mime($imagePath))->body(file_get_contents($imagePath));
			} else {
				header("HTTP/1.0 404 Not Found");
				die();
			}
		} else {
			header("HTTP/1.0 404 Not Found");
			die();
		}
	}
}