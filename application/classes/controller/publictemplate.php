<?php
/**
 * Description of public template controller
 * Controller for handling all the requests needed
 * for the public functionality.
 *
 * @author Vingi team
 * 
 * @date 29-10-2012
 */
defined('SYSPATH') or die('No direct script access.');

class Controller_Publictemplate extends Controller_Template {

  public $template = 'templates/publicDefault';

  /**
   * Initialize properties before running the controller methods (actions),
   * so they are available to our action.
   */
  public function before() {
    // Run anything that need ot run before this.
    parent::before();

    if ($this->auto_render) {
      // Initialize empty values
      $this->template->title = 'Vingi';
      $this->template->meta_keywords = '';
      $this->template->meta_description = '';
      $this->template->meta_copywrite = '';
      $this->template->header = '';
      $this->template->navigation = '';
      $this->template->content = '';
      $this->template->footer = '';
      $this->template->styles = array();
      $this->template->scripts = array();
    }
  }

  /**
   * Fill in default values for our properties before rendering the output.
   */
  public function after() {
    if ($this->auto_render) {
      // Define defaults
      $styles = array(
          'assets/css/screen.css' => 'screen',
          'assets/css/handheld.css' => 'handheld',
          'assets/css/newStyle.css' => 'screen',
          'assets/css/print.css' => 'print',
      );
      $scripts = array('assets/js/js.js',
                       'assets/js/jquery.form.js',
                       'assets/js/jquery.min.js');

      // Add return uri to session
      Session::instance()->set('return_uri', $this->request->uri());
      
      // Add defaults to template variables.
      $this->template->bind('controller', $this->request->controller());
      $this->template->bind('action', $this->request->action());
      $this->template->styles = array_reverse(array_merge($this->template->styles, $styles));
      $this->template->scripts = array_reverse(array_merge($this->template->scripts, $scripts));

      $this->template->header = new View('statics/headerPublic');
      $this->template->footer = new View('statics/footerPublic');
    }

    // Run anything that needs to run after this.
    parent::after();
  }

}
