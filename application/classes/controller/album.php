<?php
/**
 * Description of album controller
 * Controller for handling all the requests needed
 * for the album functionality.
 *
 * @author Vingi team
 * 
 * @date 29-10-2012
 */
defined('SYSPATH') or die('No direct script access.');

class Controller_Album extends Controller_PrivateTemplate {

    private $view;
    
    /**
     * If no action is given, redirect to home 
     */
    public function action_index() {
        $this->request->redirect('home');
    }
    
    /**
     * Show the album with it's info and increment 
     * the amount of album views.
     */
    public function action_view(){
        $this->view = new View('pages/album');
        
        $albumId = $this->request->param('id');
        
        $this->setAlbum($albumId);
    }
    
    /**
     * Bind the values of an album to the view
     * @param int $albumId 
     */
    private function setAlbum($albumId)
    {
        $album = Model_Album::getById($albumId);
        
        if($album->inGroup($this->user)){
            $medias = $album->getMedias();

            $group = Model_Group::getById($album->groupId);

            $this->incrementViews($albumId);
            $this->view->bind('medias', $medias);
            $this->view->bind('group', $group);
            $this->view->bind('album', $album);
            $this->view->bind('edit', $this->isOwner($album, $this->user->id ));

            $coverImage = Model_Media::getById($album->coverImage);
            $this->view->bind('cover', $coverImage);

            $this->template->content = $this->view;
            $this->template->title = "Vingi - $album->name";
            $this->template->active = 'explore';
        }else{
            $this->request->redirect('home', 200);
        }
    }
    
    /**
     * Add media to a given album 
     */
    public function action_addMedia() {
        
        $this->view = new View('pages/albumAddMedia');

        $albumId = $this->request->param('id');
        $album = Model_Album::getById($albumId);
            
        if($album->inGroup($this->user)){
            
            $albumMedias = $album->getMedias();

            $group = Model_Group::getById($album->groupId);
            $groupMedias = $album->getGroupImagesNotInAlbum();

            $this->view->bind('album', $album);
            $this->view->bind('group', $group);
            $this->view->bind('medias', $albumMedias);
            $this->view->bind('groupMedias', $groupMedias);

            $this->template->content = $this->view;
        }else{
            $this->request->redirect('home', 200);
        }
    }
    
    /**
     * Check if a user is the owner of an album
     * @param Model_Album $album
     * @param int $userId
     * @return boolean isOwner 
     */
    private function isOwner(Model_Album $album, $userId){
        $group = Model_Group::getById($album->groupId);
        return $group->ownerId == $userId;
    }

    /**
     * The create new album form page
     */
    public function action_create() {
    	$this->view = new View('pages/albumCreate');
    	$groupId = $this->request->param('id');
    	$group = Model_Group::getById($groupId);
    	
    	$this->view->bind('group', $group);
        $this->template->content = $this->view;
        $this->template->title = "Vingi - New album";
        $this->template->active = 'explore';
    }
    
    /**
     * The create album action of the form
     */
    public function action_newAlbum(){
        $albumData = $this->request->post('album');
        
        /** Create the album **/
        $album = Model_Album::factory();
        $album->name = $albumData['name'];
        $album->description = $albumData['description'];
        $album->groupId = $albumData['groupId'];
        $album->save();
        
        /** Create the album cover **/
        $media = $this->setAlbumCover($album, $_FILES['albumCover']);
        
        /** Add the album cover to the album **/
        if($media){
            $album->group->addMedia($media);
            $album->coverImage = $media->id;
            $album->save();
        }
        
        $this->request->redirect('album/view/'.$album->id);
    }
    
    /**
     * The edit album view with loaded data
     */
    public function action_edit() {
        $this->view = new View('pages/albumEdit');
        $albumId = $this->request->param('id');
        $album = Model_Album::getById($albumId);
        
        if($album->inGroup($this->user)){
            $this->view->bind('album', $album);
            $this->template->content = $this->view;
            $this->template->title = "Vingi - $album->name";
            $this->template->active = 'explore';
        }else{
            $this->request->redirect('home', 200);
        }
    }
    
    /**
     * The edit action of the edit form
     */
    public function action_changeAlbum(){
        $albumData = $this->request->post('album');
        
        /** Create the album **/
        $album = Model_Album::getById($albumData['id']);
        
        if($album->inGroup($this->user)){
            $album->name = $albumData['name'];
            $album->description = $albumData['description'];
            $album->save();

            /** Create the album cover **/
            if(isset($_FILES['albumCover'])){
                $media = $this->setAlbumCover($album, $_FILES['albumCover']);

                /** Add the album cover to the album **/
                if($media){
                    $album->group->addMedia($media);
                    $album->coverImage = $media->id;
                    $album->save();
                }
            }

            $this->request->redirect('album/view/'.$albumData['id']);
        }else{
            $this->request->redirect('home', 200);
        }
    }
    
    /**
     * Save an image as cover of the album
     * @param Model_Album $album
     * @param file $file
     * @return int $mediaId 
     */
    private function setAlbumCover(Model_Album $album, $file){
        
        if($album->inGroup($this->user)){
            if(!empty($file) && $file['error'] == 0){
                $media = Model_Media::factory();
                $media->userId = $this->user->id;
                $media->name = $album->name.' cover';
                $media->description = 'Cover of album '.$album->name;
                $media->save($file);

                return $media;
            }else{
                return false;
            }
        }else{
            $this->request->redirect('home', 200);
        }
    }
    
    /**
     * Delete action of an album
     */
    public function action_deleteAlbum(){
        $albumId = $this->request->post('id');
        $album = Model_Album::getById($albumId);
        
        if($album->inGroup($this->user)){
            $album->delete();
        }else{
            $this->request->redirect('home', 200);
        }
    }
    
    /**
     * Add media to an album 
     */
    public function action_addMediaToAlbum() {
    	$this->auto_render = false;
    	
    	$data = json_decode($this->request->body(), TRUE);	
    	
    	$album = Model_Album::getById($data['album']);
        
        if($album->inGroup($this->user)){
            $medias = $data['medias'];

            foreach($medias as $set) {
                    $user = Model_User::getByUnique($set['user']);
                    $media = Model_Media::getByUnique($set['media'], $user);

                    $album->addMedia($media);
            } 	  
        }else{
            $this->request->redirect('home', 200);
        }
    }
    
    /**
     * Remove media from an album 
     */
    public function action_removeMediaFromAlbum() {
        $this->auto_render = false;
    	
    	$data = json_decode($this->request->body(), TRUE);	
    	
    	$album = Model_Album::getById($data['album']);
        
        if($album->inGroup($this->user)){
            $medias = $data['medias'];

            foreach($medias as $set) {
                    $user = Model_User::getByUnique($set['user']);
                    $media = Model_Media::getByUnique($set['media'], $user);

                    $album->removeMedia($media);
            } 
        }else{
            $this->request->redirect('home', 200);
        }
    }
    
    /**
     * Increase the number of views for the current media item
     */
    private function incrementViews($albumId) {
        $album = Model_Album::getById($albumId);
        
        if ($album->loaded()){
            $views = $album->views + 1;
            $album->views = $views;
            $album->save();
        }
    }
        
}
