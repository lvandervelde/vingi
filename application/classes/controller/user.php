<?php
/**
 * Description of user controller
 * Controller for handling all the requests needed
 * for the user functionality.
 *
 * @author Vingi team
 * 
 * @date 29-10-2012
 */
defined('SYSPATH') or die('No direct script access.');

class Controller_User extends Controller_PrivateTemplate {

    private $view;

    public function action_index() {
        $this->request->redirect('home');
    }
    
    /**
     * Display the profile of the given user 
     */
    public function action_profile(){
        $userUnique = $this->request->param('id');
        $this->view = new View('pages/user');
        $this->setUser($userUnique);
        $this->template->content = $this->view;
    }

    /**
     * Bind the user information to the view
     * @param string $unique 
     */
    private function setUser($unique) {
        $session = Session::instance();
        $sessionUser = Model_User::getById($session->get('userId'));
        
        $edit = false;
        if (empty($unique) || $unique == $sessionUser->userUnique) {
            $edit = true;
            $this->view->bind('groups', $sessionUser->getGroups());
            $this->view->bind('uploads', $sessionUser->getMedias());
            $this->view->bind('invites', $sessionUser->getInvites());
            $user = $sessionUser;
        } 
        else {
            $user = Model_User::getByUnique($unique);
            $this->view->bind('groups', $sessionUser->getGroupsOfMutualGroupPartners($user));
            $this->view->bind('uploads', $sessionUser->getMediasOfMutualGroupsByUser($user, 7));
        }
        $this->view->bind('edit', $edit);
        $this->view->bind('user', $user);
        $this->template->title = "Vingi - $user->firstname $user->lastname";
        $this->template->active = 'profile';
        
    }

    /**
     * Show the groups the current user is a member of 
     */
    public function action_groups() {
        $this->view = new View('pages/userGroups');
        
        $this->view->bind('groups', $this->user->getGroups());
        $this->view->bind('user', $this->user);

        $this->template->content = $this->view;
        $this->template->title = "Vingi - Groups";
        $this->template->active = 'groups';
    }

    /**
     *  Show all medias of the current user
     */
    public function action_medias() {
    	$unique = $this->request->param('id');
    	
    	$this->view = new View('pages/userMedias');
    	if (empty($unique) || $unique == $this->user->userUnique) {
    		$this->view->bind('uploads', $this->user->getMedias(100, 0));
    		$user = $this->user;
    	}
    	else {
    		$user = Model_User::getByUnique($unique);
    		$this->view->bind('uploads', $user->getMediasOfMutualGroupsByUser($user, 100));
    	}
    	
    	$this->view->bind('user', $user);
    	$this->template->content = $this->view;
    }
    
    /**
     * Display the edit user form 
     */
    public function action_edit() {
        $this->view = new View('pages/userEdit');

        $session = Session::instance();
        $userId = $session->get('userId');
		
        $user = Model_User::getById($userId);
        $this->view->bind('user', $user);
        
        $this->template->content = $this->view;
        $this->template->title = "Vingi - $user->firstname $user->lastname ";
        $this->template->active = 'profile';
    }
    
    /**
     * Action from the edit user form 
     */
    public function action_changeUser() {
    	$userData = $this->request->post('user');
    	
    	$user = $this->user;
    	
    	$email = $userData['email'];
    	if(!empty($email)) {
    		$user->email = $email;
    		$user->phonenumber = $userData['phonenumber'];
    		$user->save();
    		$this->request->redirect('user/profile/');
    	}
    	else {
    		// error.
    		$this->request->redirect('user/edit/');
    	}
    }
    
    public function action_leaveGroup(){
        $groupId = $this->request->param('id');
        
        $leaveSuccess = Model_Group::getById($groupId)->removeUser($this->user);
        
        if($leaveSuccess){
            $this->request->redirect('user/groups');
        }
        
        $this->request->redirect('user/groups');
    }
    
    /**
     * Action for getting all albums where user is owner 
     */
    public function action_getAlbums(){
        $this->auto_render = false;
        $userAlbums = array();
        
        $count=0;
        foreach($this->user->groups as $group){
            echo $group->name.'<br/>';
            foreach($group->albums as $album){
                $userAlbums[$count] = $album->id;
                $userAlbums[$count] = $album->name;
                
                $count++;
            }
        }
        
        echo json_encode($userAlbums);
    }
}
