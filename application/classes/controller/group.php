<?php
/**
 * Description of group controller
 * Controller for handling all the requests needed
 * for the group functionality.
 *
 * @author Vingi team
 * 
 * @date 29-10-2012
 */
defined('SYSPATH') or die('No direct script access.');


class Controller_Group extends Controller_PrivateTemplate {

    private $view;

    /**
     * If no action is given redirect to home 
     */
    public function action_index() {  	 
        $this->request->redirect('home');
    }

    /**
     * Action to view the profile 
     */
    public function action_profile() {       	
    	$groupId = $this->request->param('id');
    	$group = Model_Group::getById($groupId);
    	
        /**
         * If the user is part of the group, display the group
         * Otherwise redirect to home.
         */
    	if($group->hasUser($this->user->id)) {
	        $this->view = new View('pages/group');
	
	        $this->display();
	
	        $this->template->content = $this->view;
    	}
    	else {
    		$this->request->redirect('home');
    	}
    }

    /**
     * Display a group and it's information.
     */
    private function display() {
    	$groupId = $this->request->param('id');
        $group = Model_Group::getById($groupId);

        $owner = Model_User::getById($group->ownerId);

        $users = $group->getUsersWithoutOwner();
        $albums = $group->getAlbums();
        $uploads = $group->getMedias(14, 0);

        $this->view->bind('uploads', $uploads);
        $this->view->bind('addAlbum', $group->isOwner($this->user->id));
        $this->view->bind('edit', $group->isOwner($this->user->id));
        $this->view->bind('invite', $group->isOwner($this->user->id));
        $this->view->bind('group', $group);
        $this->view->bind('owner', $owner);
        $this->view->bind('users', $users);
        $this->view->bind('albums', $albums);
        
        $this->template->title = "Vingi - $group->name";
        $this->template->active = 'groups';
    }
    

    /**
     * Display the edit group page
     */
    public function action_edit() {
    	$session = Session::instance();
    	$sessionId = $session->get('userId');
    	
    	$groupId = $this->request->param('id');
    	$group = Model_Group::getById($groupId);
    	 
    	// Changes may only be made by the group owner
    	if(!$group->isOwner($sessionId)) {
    		$this->request->redirect('group/profile/'.$groupId);
    	}
    	else {
	    	$this->view = new View('pages/groupEdit');
	    	$this->view->bind('group', $group);
	    	
	    	$this->template->content = $this->view;
	    	$this->template->title = "Vingi - $group->name";
	    	$this->template->active = 'groups';
    	}
    }
    
    /**
     * Action from the edit group form
     */
    public function action_changeGroup() {
    	$groupData = $this->request->post('group');
    	$groupId = $groupData['id'];
    	$group = Model_Group::getById($groupId);
    	
    	$session = Session::instance();
    	$userId = $session->get('userId');
    	 
    	$name = $groupData['name'];
    	
    	// Changes may only be made by the group owner
    	if(!$group->isOwner($userId)) {
    		$this->request->redirect('group/profile/'.$groupId);
    	}
    	else if(!empty($name)) {
    		$group->name = $name;
    		$group->description = $groupData['description'];
    		$group->save();
    		$this->request->redirect('group/profile/'.$groupId);
    	}
    	else {
    		// error.
    		$this->request->redirect('group/edit/'.$groupId);
    	}
    }
    
    /**
     * Display the change group image page
     */
    public function action_editGroupImage() {    	 
    	$groupId = $this->request->param('id');
    	$group = Model_Group::getById($groupId);
    	
    	// Changes may only be made by the group owner
    	if(!$group->isOwner($this->user->id)) {
    		$this->request->redirect('group/profile/'.$groupId);
    	}
    	else {
    		$this->view = new View('pages/groupImageEdit');
    		$this->view->bind('group', $group);
    	
    		$this->template->content = $this->view;
    		$this->template->title = "Vingi - $group->name";
    		$this->template->active = 'groups';
    	}
    }
    
    /**
     * Action for changing the group image 
     */
    public function action_changeGroupImage() {
    	$groupData = $this->request->post('group');
    	
    	/** Create the album **/
    	$group = Model_Group::getById($groupData['id']);
    	
    	/** Create the album cover **/
    	$mediaId = $this->setGroupImage($group, $_FILES['groupImage']);
    	
    	/** Add the album cover to the album **/
    	if($mediaId){
    		$group->groupImage = $mediaId;
    	}
    	$group->save();
    	$this->request->redirect('group/profile/'.$group->id);
    }
    
    /**
     * Save an image as cover of the album
     * @param Model_Album $album
     * @param file $file
     * @return int $mediaId
     */
    private function setGroupImage(Model_Group $group, $file){
    	if(!empty($file) && $file['error'] == 0){
    		$media = Model_Media::factory();
    		$media->userId = $this->user->id;
    		$media->name = $group->name.' image';
    		$media->description = 'Image of group '.$group->name;
    		$media->save($file);
    
    		return $media->id;
    	}
    	else{
    		return false;
    	}
    }
    
    /**
     * Manage the user within a group.
     * Remove users from the group or invite new users.
     */
    public function action_manage() {
    	// Group
    	$groupId = $this->request->param('id');
    	$group = Model_Group::getById($groupId);
 
    	// Check if you are the owner
    	if(!$group->isOwner($this->user->id)) {
    		$this->request->redirect('group/profile/'.$groupId);
    	}
    	else {
    		$users = $group->getUsersWithoutOwner();
    		$owner = Model_User::getById($this->user->id);
    	 
    		$this->view = new View('pages/groupManage');
    		$this->view->bind('group', $group);
    		$this->view->bind('users', $users);
    		$this->view->bind('owner', $owner);
    		
    		$this->template->content = $this->view;
    		$this->template->title = "Vingi - $group->name";
    		$this->template->active = 'groups';
    	}
    }
    
    /**
     * Display a form for inviting new users to the group 
     */
    public function action_inviteUser() {
        $groupId = $this->request->param('id');
    	$group = Model_Group::getById($groupId);
    	
    	// Invites may only send by the group owner
    	if(!$group->isOwner($this->user->id)) {
    		$this->request->redirect('group/profile/'.$groupId);
    	}
    	else {
    		$this->view = new View('pages/groupInvite');
    		$this->view->bind('group', $group);
    		
    		$this->template->content = $this->view;
    		$this->template->title = "Vingi - Invite people for $group->name";
    		$this->template->active = 'groups';
    	}
    }
    
    /**
     * Action from the invite user form 
     */
    public function action_sendInvite() {
    	$inviteData = $this->request->post('invite');
    	$groupId = $inviteData['id'];
    	$group = Model_Group::getById($groupId);
    	 
    	$session = Session::instance();
    	$userId = $session->get('userId');
    	$sessionUser = Model_User::getById($userId);
    	
    	$email = $inviteData['email'];
    	$description = $inviteData['description'];
    
    	// Invites may only send by the group owner
    	if(!$group->isOwner($userId)) {
    		$this->request->redirect('group/profile/'.$groupId);
    	}
    	else if(empty($email)) {
    		$this->request->redirect('group/inviteUser/'.$groupId);
    	}
    	else {
    		/** Invite **/
    		$invite = Model_Invite::factory();
	    	$invitedUser = Model_User::factory();
	    	$invitedUser = $invitedUser->getByEmail($email);
	    	$invitedId = $invitedUser->id;
	    	
	    	// Existing User
	    	if($invitedId != null) {	 
	    		   		
	    		   		
	    		// User is already in group
	    		if($group->hasUser($invitedId)) {
	    			print("User already in group: ".$group->name);
	    		}
	    		// User is already invited
	    		else if($invite->exists($email, $groupId)) {
	    			//var_dump($invite);
	    			print("Invite already exists: ".$invitedUser->email."/".$email);
	    		}
	    		// Invite user
	    		else {
		    		$invite->text = $description;
		    		$invite->groupId = $groupId;
		    		$invite->email = $email;
		    		$invite->save();
		    		print("Invite send to ".$invitedId);
		    		print("Message: ".$description);
	    		}
	    		$this->request->redirect('group/profile/'.$groupId);
	    	}
	    	else if($invite->exists($email, $groupId)) {
	    		//var_dump($invite);
	    		print("Invite already exists: ".$invitedUser->email."/".$email);
	    	}
	    	// New User
	    	else {
	    		$unique = $invitedUser->generateUnique();
	    		
	    		$invite->text = $description;
	    		$invite->groupId = $groupId;
	    		$invite->email = $email;
	    		$invite->inviteUnique = $unique;
	    		$invite->save();
	    		print("Invite send to ".$invitedId);
	    		print("Message: ".$description);
	    		
	    		/** Mail **/
	    		$description = $description.'<br> Click the following link: <br> http://www.vingi.nl/login/newUser/'.$unique;
	    		$mailer = new Helper_Mailer();
	    		$mailer->createMessage('Vingi', 'noreply.vingi@gmail.com', $email, $description);
	    		$mailer->sendMessage();
	    		$this->request->redirect('group/profile/'.$groupId);
	    	}
    	}
    }
    
    /**
     * Action for removing a user from the group
     */
    public function action_removeUser() {
        $removeData = $this->request->post('remove');
    	$groupId = $removeData['groupId'];
    	$group = Model_Group::getById($groupId);
    	
    	$userId = $removeData['userId'];
    	$user = Model_User::getById($userId);
    	
    	// Invites may only send by the group owner
    	if(!$group->isOwner($this->user->id)) {
    		$this->request->redirect('group/profile/'.$group->id);
    	}
    	else {
    		$this->view = new View('pages/userRemove');
    		$this->view->bind('user', $user);
    		$this->view->bind('group', $group);
    		
    		$this->template->content = $this->view;
    		$this->template->title = "Vingi - Remove users from $group->name";
    		$this->template->active = 'groups';
    	}
    }
    
    /**
     * Action for deleting a user 
     */
    public function action_deleteUser() {
    	$deleteData = $this->request->post('delete');
    	
   		$user = Model_User::getById($deleteData['userId']);
    	$group = Model_Group::getById($deleteData['groupId']);
    	
    	if($deleteData['submit'] == 'Yes') {
    		$group->removeUser($user);
    	}
    	$this->request->redirect('group/manage/'.$group->id);
    }

    /**
     * Action for creating a group 
     */
    public function create() {
        $groupData = $this->request->post('group');
		
        $group = Model_Group::factory();
        $group->name = $groupData['name'];
        $group->description = $groupData['description'];
        $group->ownerId = $groupData['ownerId'];
        
        $mediaId = $this->setGroupImage($group, $_FILES['groupImage']);
        
        /** Add the group image to the group **/
        if($mediaId){
            $group->groupImage = $mediaId;
        }
        
        $group->save();
    } 
}
