<?php
/**
 * Description of private template controller
 * Controller for handling all the requests needed
 * for the private functionality.
 *
 * @author Vingi team
 * 
 * @date 29-10-2012
 */
defined('SYSPATH') or die('No direct script access.');

class Controller_Privatetemplate extends Controller_Template {

  public $template = 'templates/privateDefault';
  public $user;
  
  
  /**
   * Initialize properties before running the controller methods (actions),
   * so they are available to our action.
   */
  public function before() {
    // Run anything that need ot run before this.
    parent::before();

    $userId = Session::instance()->get('userId');

    if(empty($userId)) {
    	$this->request->redirect('login', 200);
    } else {
    	$this->user = Model_User::getById($userId);
    }
    
    if ($this->auto_render) {
      // Initialize empty values
      $this->template->title = 'Vingi';
      $this->template->meta_keywords = '';
      $this->template->meta_description = '';
      $this->template->meta_copywrite = '';
      $this->template->header = '';
      $this->template->navigation = '';
      $this->template->content = '';
      $this->template->footer = '';
      $this->template->styles = array();
      $this->template->scripts = array();
      $this->template->active = '';
    }
  }

  /**
   * Fill in default values for our properties before rendering the output.
   */
  public function after() {
    if ($this->auto_render) {
      // Define defaults
      $styles = array(
          'assets/css/jquery-ui-custom.css' => 'screen',
          'assets/css/newStyle.css' => 'screen',
          'assets/css/screen.css' => 'screen',
          'assets/css/handheld.css' => 'handheld',
          'assets/css/print.css' => 'print'
      );
      $scripts = array(
          'assets/js/js.js',
          'assets/js/jquery-ui.min.js',
          'assets/js/jquery.min.js'
      );

      // Add return uri to session
      Session::instance()->set('return_uri', $this->request->uri());
      
      // Add defaults to template variables.
      $this->template->bind('controller', $this->request->controller());
      $this->template->bind('action', $this->request->action());
      $this->template->styles = array_reverse(array_merge($this->template->styles, $styles));
      $this->template->scripts = array_reverse(array_merge($this->template->scripts, $scripts));

      $navigation = new View('statics/navigation');
      $navigation->bind('active', $this->template->active);
      $this->template->navigation = $navigation;
      
      $header = new View('statics/headerPrivate');
      $header->bind('active', $this->template->active);
      $this->template->header = $header;
      
      $this->template->footer = new View('statics/footerPrivate');
    }

    // Run anything that needs to run after this.
    parent::after();
  }
}
