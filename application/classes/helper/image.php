<?php defined('SYSPATH') or die('No direct script access.');

class Helper_Image 
{
	/**
	 * 
	 * Creates the path to for the given unique. 
	 * This is used to find the saved media on the server.
	 * 
	 * @param string $unique
	 */
	public static function createDirectory($unique) {
		return $directory = implode(str_split($unique, 3), '/').'/';
	}
}