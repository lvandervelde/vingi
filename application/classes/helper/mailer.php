<?php error_reporting(E_ALL);

require_once ('C:/xampp/htdocs/vingi/assets/lib/Swift-4.1.1/swift_required.php');

class Helper_Mailer
{
	public $message = '';
	
	public function __construct($username = 'noreply.vingi@gmail.com', $password = 'vingipass42') {
		$this->mailer = Swift_Mailer::newInstance(Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, "ssl")
													->setUsername($username)
													->setPassword($password));
													
		return $this->mailer;
	}
	
	public function getMailer() {
		return $this->mailer;
	}
	
	public function createMessage($title, $from, $to, $body) {
		$this->message = Swift_Message::newInstance($title)
					->setFrom($from)
					->setTo($to)
					->setBody($body)
					->setContentType("text/html");
	}
	
	public function sendMessage() {
		$this->mailer->send($this->message);
	}
}