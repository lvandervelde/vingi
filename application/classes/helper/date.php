<?php defined('SYSPATH') or die('No direct script access.');

class Helper_Date
{
	/**
	 * 
	 * Generates a nice date string based on the date given.
	 * 
	 * @param string $date
	 */
	public static function niceDate($date) {
		return date("j F Y", strtotime($date));
	}
	
	/**
	 * 
	 * Generates a nice datetime string based on the datetime given
	 * 
	 * @param string $datetime
	 */
	public static function niceDateTime($datetime) {
		return date("j M Y, g:i a",strtotime($datetime));
	}
}