<?php defined('SYSPATH') or die('No direct script access.');

class Helper_Bcrypt 
{
	
	private $rounds;
	
	/**
	 * 
	 * Constructs a BCrypt class to generate hashes.
	 * The controller sets the round value to slow the hasing script down.
	 * 
	 * @param int $rounds
	 */
	public function __construct($rounds = 12) {
		$this->rounds = $rounds;
	}

	/**
	 * 
	 * Generates a salt to be used by the Generated hash function. 
	 *
	 */
	public function genSalt() {
		$seed = '';
		for($i = 0; $i < 16; $i++) {
			$seed .= chr(mt_rand(0, 255));
		}

		$salt = substr(strtr(base64_encode($seed), '+', '.'), 0, 22);
		return $salt;
	}

	/**
	 * 
	 * Generates a hash for the given password.
	 * 
	 * @param string $password
	 */
	public function genHash($password) {
		$hash = crypt($password, '$2a$' . $this->rounds . '$' . $this->genSalt());
		return $hash;
	}

	/**
	 * 
	 * Checks if the hash and password do match.
	 * 
	 * @param string $password
	 * @param string $existingHash
	 */
	public function verify($password, $existingHash) {

		$hash = crypt($password, $existingHash);

		if($hash === $existingHash) {
			return true;
		} else {
			return false;
		}
	}
}