<?php

class CommentTest extends Kohana_UnitTest_TestCase
{
	
    public function testCreateGetDeleteComment()
    {
    	// Create a comment
        $comment = Model_Comment::factory();
       	$comment->text = "A test comment.";
       	$comment->userId = 1;
       	$comment->mediaId = 1;

        // Save and check if comment has been saved correctly
        $comment->save();
        $this->assertNotEmpty($comment->id);

        // Check if we can get a comment by id
        $getComment = Model_Comment::getById($comment->id);
        $this->assertTrue($getComment->loaded());

        // Check if the album can be deleted.
        $getComment->delete();
        $this->assertNull($getComment->id);	
    } 

    public function testGetCommentByMedia()
    {
    	$media = Model_Media::factory();
    	$media->name = "Test";
    	$media->description = "Test";
    	$media->size = 0;
    	$media->width = 0;
    	$media->height = 0;
    	
    	$media->save();
    	
    	$this->assertNotNull($media->id);
    	
    	$comment = Model_Comment::factory();
    	$comment->text = "A test comment.";
    	$comment->userId = 1;
    	$comment->mediaId = $media->id;
    	
    	$comment2 = Model_Comment::factory();
    	$comment2->text = "Second Comment";
    	$comment2->userId = 1;
    	$comment2->mediaId = $media->id;
    	
    	// Save and check if comment has been saved correctly
    	$comment->save();
    	$comment2->save();
    	
    	$this->assertNotNull($comment->id);
    	$this->assertNotNull($comment2->id);
    	
    	$result = Model_Comment::getByMediaId($media->id);
    	
    	$comments = array();
    	
    	foreach($result as $comment) {
    		array_push($comments, $comment);
    	}
    	
    	$this->assertEquals(count($comments), 2);
    	
    	$media->delete();
    	$comment->delete();
    	$comment2->delete();
    }
}