<?php

class MediaTest extends Kohana_UnitTest_TestCase
{
    /**
     * Test the creation and removing of a media object 
     */
    public function testCreateGetDeleteMedia()
    {
    	// Create a comment
        $media = Model_Media::factory();
        $media->mediaUnique = 'u1n2i3q4u5e6';
        $media->name = 'medianame';
        $media->description = 'description';
        $media->userId = '1';
        $media->typeId = '1';
        $media->width = '500';
        $media->height = '600';
        $media->size = '700';
        $media->views = '0';

        // Save and check if media has been saved correctly
        $media->save();
        $this->assertNotEmpty($media->id);

        // Check if we can get a media by id
        $getMedia = Model_Media::getById($media->id);
        $this->assertTrue($getMedia->loaded());

        // Check if the media can be deleted.
        $getMedia->delete();
        $this->assertNull($getMedia->id); 
    }   
    
    /**
     * Test the adding and removing of a tag object
     * from a media object 
     */
    public function testAddGetRemoveTag()
    {
        // Create a comment
        $media = Model_Media::factory();
        $media->mediaUnique = 'u1n2i3q4u5e6';
        $media->name = 'medianame';
        $media->description = 'description';
        $media->userId = '1';
        $media->typeId = '1';
        $media->width = '500';
        $media->height = '600';
        $media->size = '700';
        $media->views = '0';

        // Save and check if media has been saved correctly
        $media->save();
        $this->assertNotEmpty($media->id);
        
        // Create a tag
        $tag = Model_Tag::factory();
        $tag->name = "TestTag";
        
        // Save the Tag 
        $tag->save();
        
        // Add Tag to media
        $media->addTag($tag);
        $this->assertTrue($media->has('tags', $tag));
        
        // Remove Tag from media
        $media->removeTag($tag);
        $this->assertFalse($media->has('tags', $tag));
        
        $tag->delete();
        $this->assertNull($tag->id);
        
        $media->delete();
        $this->assertNull($media->id); 
    }
    
    /**
     * Test the adding and removing of a comment object
     * from a media object 
     */
    public function testAddGetRemoveComment()
    {
        // Create a comment
        $media = Model_Media::factory();
        $media->mediaUnique = 'u1n2i3q4u5e6';
        $media->name = 'medianame';
        $media->description = 'description';
        $media->userId = '1';
        $media->typeId = '1';
        $media->width = '500';
        $media->height = '600';
        $media->size = '700';
        $media->views = '0';

        // Save and check if media has been saved correctly
        $media->save();
        $this->assertNotEmpty($media->id);
        
        // Create a Comment
        $comment = Model_Comment::factory();
       	$comment->text = "A test comment.";
       	$comment->userId = 1;
       	$comment->mediaId = $media->id;
        
        // Save the Comment
        $comment->save();
                
        $hasComment = false;
        
        // Get all Comments from Media
        foreach($media->getComments() as $commentObj){
            if($commentObj->id == $comment->id){
                $hasComment = true;
            }
        } 
        
        $this->assertTrue($hasComment);
        
        // Save commentId for check
        $commentId = $comment->id;
        
        // Remove Tag from media
        $media->removeComment($comment);
        
        $hadComment = true;
        // Get all Comments from Media
        foreach($media->getComments() as $commentObj){
            if($commentObj->id == $commentId){
                $hadComment = false;
            }
        } 
        
        $this->assertTrue($hadComment);
        
        $media->delete();
        $this->assertNull($media->id); 
    }
}
