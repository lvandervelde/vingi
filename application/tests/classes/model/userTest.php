<?php

class UserTest extends Kohana_UnitTest_TestCase
{
	
    public function testCreateGetDeleteUser()
    {
    	// Create an User
        $user = Model_User::factory();
        $user->email = "test@test.nl";
        $user->password = "test";
        $user->firstname = "Test";
        $user->lastname = "Test";
        $user->gender = 0;
        $user->userUnique = "123456789012";
        $user->phonenumber = "0623230000";
        
        // Save the User and check if album id is not empty.
        $user->save();
        $this->assertNotEmpty($user->id);
        
        // Check if User can get by Id.
        $getUser = Model_User::getById($user->id);
        $this->assertTrue($getUser->loaded());
        
        // Check if User can get by Unique.
        $getUser = Model_User::getByUnique($user->userUnique);
        $this->assertTrue($getUser->loaded());
        
        // Check if user can get by Email
        $getUser = Model_User::getByEmail($user->email);
        $this->assertTrue($getUser->loaded());
        
        // Check if the User can be deleted.
        $getUser->delete();
        $this->assertNull($getUser->id);  
    } 

    public function testGetGender() 
    {
    	// Create an User
    	$user = Model_User::factory();
    	$user->email = "test@test.nl";
    	$user->password = "test";
    	$user->firstname = "Test";
    	$user->lastname = "Test";
    	$user->gender = 0;
    	$user->userUnique = "123456789012";
    	$user->phonenumber = "0623230000";
    	
    	$user->save();
    	
    	$this->assertEquals($user->getGender(), "Male");
    	$this->assertEquals($user->getAvatar(), "assets/img/avatar.jpg");
   
  		$user->gender = 1;
  		$user->save();
    	
  		$this->assertEquals($user->getGender(), "Female");
  		$this->assertEquals($user->getAvatar(), "assets/img/avatar_female.jpg");
  		
    	$user->delete();	
    }

    public function testGenerateUnique()
    {
    	$generateUnique = Model_User::generateUnique();
    	
    	$this->assertNotNull($generateUnique);
    	$this->assertEquals(strlen($generateUnique), 12);
    }
}