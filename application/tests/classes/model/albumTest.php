<?php

class AlbumTest extends Kohana_UnitTest_TestCase
{

	public function testCreateGetDeleteAlbum()
	{
		// Create a Group
		$group = Model_Group::factory();
		$group->name = "Test Group";
		$group->description = "Test Group";

		$group->save();
		
		// Create an Album
		$album = Model_Album::factory();
		$album->name = "Test Album";
		$album->description = "Album";
		$album->groupId = $group->id;
		// Save the Album and check if album id is not empty.
		$album->save();
		
		$this->assertNotEmpty($album->id);

		$this->assertTrue($album->loaded());

		// Check if the album can be deleted.
		$album->delete();
		$this->assertNull($album->id);
	}

	public function testInGroupNotInGroup()
	{
		// Create a Group
		$group = Model_Group::factory();
		$group->name = "Test Group";
		$group->description = "Test Group";

		$group->save();
			
		// Create an Album
		$album = Model_Album::factory();
		$album->name = "Test Album";
		$album->description = "Test Album";
		$album->groupId = $group->id;
			
		$album->save();

		$group->addAlbum($album);
			
		$user = Model_User::factory();
		$user->email = "test@test.nl";
		$user->password = "test";
		$user->firstname = "Test";
		$user->lastname = "Test";
		$user->gender = 0;
		$user->userUnique = "123456789012";
		$user->phonenumber = "0623230000";

		$user->save();

		$group->addUser($user);

		$this->assertTrue($album->inGroup($user));

		$group->removeUser($user);

		$group->removeAlbum($album);

		$this->assertFalse($album->inGroup($user));

		$album->delete();
		$this->assertNull($album->id);
		
		$user->delete();
		$this->assertNull($user->id);
		
		$group->delete();
		$this->assertNull($group->id);
	}

	public function testAddRemoveMedia()
	{
		// Create a Group
		$group = Model_Group::factory();
		$group->name = "Test Group";
		$group->description = "Test Group";

		$group->save();

		// Create an Album
		$album = Model_Album::factory();
		$album->name = "Test Album";
		$album->description = "Test Album";
		$album->groupId = $group->id;
			
		$album->save();

		$media = Model_Media::factory();
		$media->mediaUnique = 'u1n2i3q4u5e6';
		$media->name = 'medianame';
		$media->description = 'description';
		$media->userId = '1';
		$media->typeId = '1';
		$media->width = '500';
		$media->height = '600';
		$media->size = '700';
		$media->views = '0';
		// Save and check if media has been saved correctly
		$media->save();
			
		$this->assertTrue($album->addMedia($media));

		$this->assertTrue($album->has("medias", $media));
		
		$this->assertTrue($album->removeMedia($media));

		$this->assertFalse($album->has("medias", $media));
		
		$album->delete();
		$this->assertNull($album->id);
		
		$media->delete();
		$this->assertNull($media->id);
		
		$group->delete();
		$this->assertNull($group->id);
	}
	
	public function testGetMedia()
	{
		// Create a Group
		$group = Model_Group::factory();
		$group->name = "Test Group";
		$group->description = "Test Group";
		
		$group->save();
		
		// Create an Album
		$album = Model_Album::factory();
		$album->name = "Test Album";
		$album->description = "Test Album";
		$album->groupId = $group->id;
			
		$album->save();
		
		$media = Model_Media::factory();
		$media->mediaUnique = 'u1n2i3q4u5e6';
		$media->name = 'medianame';
		$media->description = 'description';
		$media->userId = '1';
		$media->typeId = '1';
		$media->width = '500';
		$media->height = '600';
		$media->size = '700';
		$media->views = '0';
		// Save and check if media has been saved correctly
		$media->save();

		$this->assertTrue($album->addMedia($media));
		
		$albumMedias = $album->getMedias();
		
		foreach($albumMedias as $albumMedia){
			$this->assertTrue($albumMedia->id == $media->id); 
		}
		
		$this->assertTrue($album->removeMedia($media));
		
		$this->assertFalse($album->has("medias", $media));
		
		$album->delete();
		$this->assertNull($album->id);
		
		$media->delete();
		$this->assertNull($media->id);
		
		$group->delete();
		$this->assertNull($group->id);
	}
}