<?php

class AlbumTag extends Kohana_UnitTest_TestCase
{
	
    public function testCreateGetDeleteTag()
    {
    	// Create an Tag
        $tag= Model_Tag::factory();
        $tag->name = "TestTag";
        
        // Save the Tag and check if album id is not empty.
        $tag->save();
        $this->assertNotEmpty($tag->id);
        
        // Check if Tag can get by Id.
        $getTag = Model_Album::getById($tag->id);
        $this->assertTrue($getTag->loaded());
        
        // Check if the Tag can be deleted.
        $getTag->delete();
        $this->assertNull($getTag->id);  
    }  
    
    public function testTagLike() {
    	
    	// Create an Tag
    	$tag= Model_Tag::factory();
    	$tag->name = "TestTag";
    	
    	$tag->save();
    	
    	$tags = Model_Tag::getTagsLike('estTa');
    	
    	$check = false;
    	foreach($tags as $t) {
    		if($t->id == $tag->id) {
    			$check = true;
    		}
    	}
    	
    	$this->assertTrue($check);	
    }  
}