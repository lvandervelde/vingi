<?php

class GroupTest extends Kohana_UnitTest_TestCase
{
	
    public function testGetGroupInfo()
    {
    	// Create a Group
        $group = Model_Group::factory();
        $group->name = "Test Group";
        $group->description = "Group description";
        
        // Save the group and check if group id is not empty.
        $group->save();
        $this->assertNotEmpty($group->id);
        
        // Check if group can get by Id.
        $getGroup = Model_Group::getById($group->id);
        $this->assertTrue($getGroup->loaded());
        
        // Check if the group has a description.
        $groupDescription = $getGroup->description;
        $this->assertEquals($groupDescription, "Group description"); 

        $group->delete();
    }    
    
    public function testIsOwner() {
    	$group = Model_Group::factory();
    	$group->name = "Test Group";
    	$group->description = "Group description";
    	$group->ownerId = 2;
    	$group->save();
    	
    	$this->assertFalse($group->isOwner(1));
    	$this->assertTrue($group->isOwner(2));
    	
    	$group->delete();
    }
}