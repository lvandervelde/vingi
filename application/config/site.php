<?php defined('SYSPATH') OR die('No direct access allowed.');

return array 
(
	// Used for the Bcrypt class
	'costFactor' => 12,
		
	// Allowed images that can be uploaded
	'allowedImages' => array('gif', 'jpeg', 'jpg', 'png'),
		
	// Image types
	'imageTypes' => array('thumb', 'small', 'large'),
	
	// Amount of pixels for the largest side for each type of picture.
	'thumbnail' => 128,
	'small' => 300,
	'large' => 1080,
	
	// Directories
	'imageRoot' => DOCROOT.'medias\\'
);
